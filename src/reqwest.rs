use crate::configuration::Configuration;
use crate::errors::InternalError;
use log::info;
use std::fs;

/// Init the Reqwest Client
///
/// # Arguments
/// * `config` - The configuration object
///
/// # Return value
/// The Reqwest Client
pub fn init_reqwest_client(config: &Configuration) -> Result<reqwest::Client, InternalError> {
    info!("Initializing reqwest client...");
    if let Some(tls) = &config.bouche.keycloak.tls {
        let mut client_builder = reqwest::ClientBuilder::new();
        if let Some(root_cert) = &tls.root_cert {
            let ca_string = fs::read_to_string(root_cert.as_str())
                .map_err(|e| InternalError::from_io(e, root_cert.as_str()))?;
            let ca = reqwest::Certificate::from_pem(ca_string.as_bytes())?;
            client_builder = client_builder.add_root_certificate(ca);
        }

        if let Some(identity) = &tls.identity {
            let crt = fs::read_to_string(identity.crt.as_str())
                .map_err(|e| InternalError::from_io(e, identity.crt.as_str()))?;
            let key = fs::read_to_string(identity.key.as_str())
                .map_err(|e| InternalError::from_io(e, identity.key.as_str()))?;
            let ca = fs::read_to_string(identity.ca.as_str())
                .map_err(|e| InternalError::from_io(e, identity.ca.as_str()))?;
            let ca_parsed = reqwest::Certificate::from_pem(ca.as_bytes())?;
            let identity =
                reqwest::Identity::from_pem(format!("{}\n{}\n{}", key, crt, ca).as_bytes())?;
            client_builder = client_builder
                .identity(identity)
                .add_root_certificate(ca_parsed);
        }
        Ok(client_builder.build()?)
    } else {
        Ok(reqwest::Client::new())
    }
}
