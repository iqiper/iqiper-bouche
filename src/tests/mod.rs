mod http;
use crate::bouche::BoucheMetrics;
use crate::configuration::Configuration;
use peau::actix_web_prom::PrometheusMetrics;
const CONF_FILE: &str = "IQIPER_BOUCHE_CONFIG";
const ENV_CONF_VAR: &str = "IQIPER_BOUCHE";
use once_cell::sync::Lazy;

static OEIL_METRICS: Lazy<BoucheMetrics> = Lazy::new(|| BoucheMetrics::new().unwrap());
static OEIL_METRICS_HTTP: Lazy<PrometheusMetrics> =
    Lazy::new(|| PrometheusMetrics::new("test", Some("/-/metrics"), None));

fn get_config() -> Configuration {
    crate::configuration::get(
        std::path::Path::new(
            std::env::var(CONF_FILE)
                .unwrap_or("./config.yml".to_string())
                .as_str(),
        ),
        ENV_CONF_VAR,
    )
    .expect("Failed to get configuration")
}
