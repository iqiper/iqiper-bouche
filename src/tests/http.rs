use super::*;
use crate::bouche::*;
use crate::errors::InternalError;
use crate::iqiper_bouche_app;
use chrono::Duration;
use peau::actix_web;
use peau::actix_web::test;
use uuid::Uuid;

#[tokio::test]
async fn test_add_task_ok() -> Result<(), InternalError> {
    let mut config = get_config();
    let db = crate::bouche::tests::lock_db().await;
    config.visage.redis.database = Some((*db).to_string());
    let redis_comm = std::sync::Arc::new(
        crate::http::RedisCommunicator::new(
            &config.visage.redis,
            config.bouche.backlog_name.clone(),
        )
        .await?,
    );
    let mut app = test::init_service(iqiper_bouche_app!(
        config,
        redis_comm,
        OEIL_METRICS_HTTP.clone()
    ))
    .await;
    let id = Uuid::new_v4();
    let req = test::TestRequest::post()
        .uri(format!("/communication/{}", id).as_str())
        .header("Content-Type", "application/json")
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    let mut master_bouche = Bouche::new(config.clone(), OEIL_METRICS.clone())
        .await
        .expect("the bouche should've been built correctly");
    master_bouche
        .visage_mut()
        .register_lease()
        .await
        .expect("should be registered");
    let (new_task, _dur) = master_bouche
        .get_new_task(Duration::seconds(0))
        .await
        .expect("the new task to be fetched");
    assert_eq!(new_task.is_some(), true, "Should be some");
    assert_eq!(new_task.unwrap(), id, "Ids should match");
    assert!(resp.status().is_success());
    Ok(())
}

#[tokio::test]
async fn test_add_task_bad_format() -> Result<(), InternalError> {
    let mut config = get_config();
    let db = crate::bouche::tests::lock_db().await;
    config.visage.redis.database = Some((*db).to_string());
    let redis_comm = std::sync::Arc::new(
        crate::http::RedisCommunicator::new(
            &config.visage.redis,
            config.bouche.backlog_name.clone(),
        )
        .await?,
    );
    let mut app = test::init_service(iqiper_bouche_app!(
        config,
        redis_comm,
        OEIL_METRICS_HTTP.clone()
    ))
    .await;
    let req = test::TestRequest::post()
        .uri(format!("/communication/{}", "AAAAAAAAAAAAAAAAAAAAAAA".to_string()).as_str())
        .header("Content-Type", "application/json")
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    let mut master_bouche = Bouche::new(config.clone(), OEIL_METRICS.clone())
        .await
        .expect("the bouche should've been built correctly");
    master_bouche
        .visage_mut()
        .register_lease()
        .await
        .expect("should be registered");
    let (new_task, _dur) = master_bouche
        .get_new_task(Duration::seconds(0))
        .await
        .expect("the new task to be fetched");
    assert_eq!(new_task.is_none(), true, "Should be some");
    assert!(resp.status().is_client_error());
    Ok(())
}
