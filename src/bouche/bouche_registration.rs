use super::*;

impl<'a> Bouche<'a> {
    /// Check if there is some orphaned `bouche`. Distribute their tasks if there is some.
    ///
    /// # Return value
    /// A list of id of dead `bouche`
    pub async fn master_check_orphaned_bouche(&self) -> Result<Vec<Uuid>, InternalError> {
        debug!("Checking for orphaned Bouche");
        let registrar: Vec<String> = self
            .visage()
            .apply_cmd_in_redis(
                visage::redis::cmd("ZRANGE")
                    .arg(BOUCHE_REGISTRAR)
                    .arg(0 as usize)
                    .arg(-1 as i64),
            ) // TODO Use ZSCAN
            .await?;
        let mut check_command = visage::redis::pipe();
        for bouche_id in registrar.iter() {
            check_command.exists(visage::VISAGE_ID_FORMAT!(bouche_id));
        }
        let existing_bouche_map: Vec<bool> = self
            .visage()
            .apply_pipe_in_redis(&mut check_command)
            .await?;
        let mut res: Vec<Uuid> = vec![];
        for (bouche_id_formatted, exists) in registrar.iter().zip(existing_bouche_map.iter()) {
            if !exists {
                match Uuid::from_str(bouche_id_formatted) {
                    Ok(id) => res.push(id),
                    Err(_e) => {
                        self.visage()
                            .apply_cmd_in_redis(&mut visage::redis::Cmd::zrem(
                                BOUCHE_REGISTRAR,
                                bouche_id_formatted,
                            ))
                            .await?
                    }
                };
            }
        }
        for bouche_id in res.iter() {
            let nb_modified = self
                .master_merge_backlogs(
                    &BOUCHE_BACKLOG_FORMAT!(bouche_id),
                    &self.config().backlog_name,
                )
                .await?;
            self.visage().unregister_lease(Some(*bouche_id)).await?;
            debug!(
                "Reassigning {} tasks from {} backlog",
                nb_modified, bouche_id
            );
        }
        debug!(
            "Delete {} bouche(s){}{}",
            res.len(),
            match res.len() {
                0 => "",
                _ => " :\n\t- ",
            },
            res.iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join("\n\t- ")
        );
        Ok(res)
    }
}
