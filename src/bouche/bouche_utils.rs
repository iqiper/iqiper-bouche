use super::*;

impl<'a> Bouche<'a> {
    /// Get a new task from the backlog
    ///
    /// # Arguments
    /// * `timeout` - The maximum to watch the backlog for new task.
    /// If 0 seconds, then it'll return imediatly if there is no task to run
    ///
    /// # Return value
    /// The task or None
    pub(crate) async fn get_new_task(
        &self,
        timeout: Duration,
    ) -> Result<(Option<Uuid>, Duration), InternalError> {
        let unparsed: Option<String>;
        let now = Utc::now();
        if timeout.num_milliseconds() != 0 {
            unparsed = self
                .visage()
                .apply_cmd_in_redis::<Option<String>>(
                    &mut visage::redis::cmd("BRPOPLPUSH")
                        .arg(self.config().backlog_name.as_str())
                        .arg(BOUCHE_BACKLOG_FORMAT!(self.visage().id()).as_str())
                        .arg(format!(
                            "{:.5}",
                            (timeout.num_milliseconds() as f64 / 1000 as f64)
                        )),
                )
                .await?;
        } else {
            unparsed = self
                .visage()
                .apply_cmd_in_redis::<Option<String>>(&mut visage::redis::Cmd::rpoplpush(
                    self.config().backlog_name.as_str(),
                    BOUCHE_BACKLOG_FORMAT!(self.visage().id()).as_str(),
                ))
                .await?;
        }
        let end = Utc::now();
        let time_spent_waiting = end - now;
        match unparsed {
            Some(unparsed) => match Uuid::from_str(unparsed.as_str()) {
                Ok(x) => Ok((Some(x), time_spent_waiting)),
                Err(_x) => {
                    warn!("Bad task id ({}) in backlog, deleting...", unparsed);
                    self.delete_task(BOUCHE_BACKLOG_FORMAT!(self.visage().id()), unparsed)
                        .await?;
                    Ok((None, time_spent_waiting))
                }
            },
            None => Ok((None, time_spent_waiting)),
        }
    }

    /// Get the communication object from the database
    ///
    /// # Arguments
    /// * `db_conn` - The database connection
    /// * `communication_id` - The id of the communication
    ///
    /// # Return value
    /// The communication object if found
    pub(super) async fn get_communication(
        &self,
        db_conn: &diesel::PgConnection,
        communication_id: &Uuid,
    ) -> Result<Option<SelectCommunication>, InternalError> {
        let res = db::communication::select_by_id_unlogged(db_conn, communication_id);
        match res {
            Ok(x) => Ok(Some(x)),
            Err(e) => match e {
                db::error::IqiperDatabaseError::DatabaseError(diesel::result::Error::NotFound) => {
                    Ok(None)
                }
                _ => Err(InternalError::DBError(e)),
            },
        }
    }

    /// Delete a task from a list in redis
    ///
    /// # Arguments
    /// * `key` - The list from which to delete
    /// * `val` - The value of the key to delete
    ///
    /// # Return value
    /// True on deletion
    pub(crate) async fn delete_task(
        &self,
        key: String,
        val: String,
    ) -> Result<bool, InternalError> {
        let tmp: usize = self
            .visage()
            .apply_cmd_in_redis(&mut visage::redis::Cmd::lrem(key, 1, val))
            .await?;
        Ok(tmp != 0)
    }

    /// Merge 2 `bouche` backlogs, useful when distributing tasks from a dead `bouche`
    ///
    /// # Arguments
    /// * `old` - The list from which to fetch the tasks
    /// * `new` - The list in which to put the tasks
    ///
    /// # Return value
    /// The number of tasks moved
    pub(crate) async fn master_merge_backlogs(
        &self,
        old: &String,
        new: &String,
    ) -> Result<usize, InternalError> {
        let mut res = 0;

        while {
            let tmp: Option<String> = self
                .visage()
                .apply_cmd_in_redis::<Option<String>>(&mut visage::redis::Cmd::rpoplpush(
                    old.as_str(),
                    new.as_str(),
                ))
                .await?;

            if tmp.is_some() {
                res = res + 1;
                true
            } else {
                false
            }
        } {}
        Ok(res)
    }

    /// Add a task to the delayed list
    ///
    /// # Arguments
    /// * `task_id` - The id of the task to execute later
    ///
    /// # Return value
    /// Nothing
    pub(crate) async fn add_to_delayed_list(&self, task_id: Uuid) -> Result<(), InternalError> {
        self.visage()
            .apply_cmd_in_redis::<u64>(&mut visage::redis::Cmd::hset(
                BOUCHE_DELAYED_BACKLOG_FORMAT!(self.visage().id()),
                task_id.to_string(),
                serde_json::to_string(&BoucheDelayedTask::new(&self.config(), task_id))?,
            ))
            .await?;
        Ok(())
    }
    /// Get the template data for a task
    ///
    /// # Arguments
    /// * `task_id` - The id of the task to execute later
    ///
    /// # Return value
    /// Nothing
    pub(crate) async fn get_template_data(
        &self,
        conn: &diesel::PgConnection,
        task: &BoucheTask,
    ) -> Result<BoucheMessageTemplateData<'_>, InternalError> {
        let activity: Option<db::activity::SelectActivity>;
        let activity_constraint: Option<db::activity_constraint::SelectActivityConstraint> =
            match db::activity_constraint_communication::select_constraint_by_communication(
                &conn,
                &task.communication().uid,
                &task.communication().id,
            ) {
                Ok(x) => Some(x),
                Err(db::error::IqiperDatabaseError::DatabaseError(
                    diesel::result::Error::NotFound,
                )) => None,
                Err(x) => return Err(InternalError::from(x)),
            };
        let tmp_activity = match &activity_constraint {
            Some(activity_constraint) => db::activity::select_by_id(
                &conn,
                &task.communication().uid,
                &activity_constraint.activity_id,
            ),
            None => db::activity_communication::select_activity_by_communication(
                &conn,
                &task.communication().uid,
                &task.communication().id,
            ),
        };
        activity = match tmp_activity {
            Ok(x) => Some(x),
            Err(db::error::IqiperDatabaseError::DatabaseError(diesel::result::Error::NotFound)) => {
                None
            }
            Err(x) => return Err(InternalError::from(x)),
        };
        let user = match self.kcc().search_user_by_id(task.communication().uid).await {
            Ok(user) => Some(user),
            Err(kcc::KCCError::NotFound(_x)) => {
                warn!(
                    "The user {} doesn't exists anymore in Keycloak for communication {}",
                    task.communication().uid,
                    task.communication().id
                );
                None
            }
            Err(err) => {
                error!("There was a Keycloak error while trying to fetch an user email (user : {}, communication : {}) : {}", task.communication().uid, task.communication().id, err);
                None
            }
        };
        Ok(BoucheMessageTemplateData::new(
            task.communication().clone(),
            user,
            activity,
            activity_constraint,
        ))
    }

    /// Remove from delayed list
    ///
    /// # Arguments
    /// * `task_id` - The id of the task to execute later
    ///
    /// # Return value
    /// Nothing
    pub(crate) async fn remove_from_delayed_list(
        &self,
        task_id: Uuid,
    ) -> Result<(), InternalError> {
        self.visage()
            .apply_cmd_in_redis::<u64>(&mut visage::redis::Cmd::hdel(
                BOUCHE_DELAYED_BACKLOG_FORMAT!(self.visage().id()),
                task_id.to_string(),
            ))
            .await?;
        self.delete_task(
            BOUCHE_BACKLOG_FORMAT!(self.visage().id()),
            task_id.to_string(),
        )
        .await?;
        Ok(())
    }

    /// Check if the bouche task fetched from redis is valid
    ///
    /// # Arguments
    /// * `res` - The result to fill
    /// * `redis_token` - The `todo` pipe to use to set some query later
    /// * `list` - The list from which to fetch the tasks
    /// * `key` - The key to fetch from that list
    /// * `task` - The raw task that was fetch using the `key`
    ///
    /// # Return Value
    /// The parsed tasked or None
    pub(crate) async fn slave_parse_delayed_task(
        &self,
        res: &mut BoucheTaskResult,
        redis_todo: &mut visage::redis::Pipeline,
        list: &String,
        key: &String,
        task: &String,
    ) -> Result<Option<BoucheDelayedTask>, InternalError> {
        let key = match Uuid::from_str(key.as_str()) {
            // Extract the key
            Ok(x) => x,
            Err(_e) => {
                res.add_failed();
                warn!(
                    "Badly formatted key ('{}') in bouche {} backlog",
                    key.as_str(),
                    self.visage().id()
                );
                redis_todo.hdel(list.as_str(), key.as_str());
                return Ok(None);
            }
        };
        let task: BoucheDelayedTask = match serde_json::from_str(task.as_str()) {
            // Extract the task
            Ok(x) => x,
            Err(_e) => {
                res.add_failed();
                warn!(
                    "Badly formatted object ('{}') in bouche {} backlog",
                    key.to_string(),
                    self.visage().id()
                );
                redis_todo.hdel(list.as_str(), key.to_string());
                return Ok(None);
            }
        };
        if key != *task.id() {
            res.add_failed();
            warn!(
                "Badly formatted object ('{}') in bouche {} backlog",
                key.to_string(),
                self.visage().id()
            );
            redis_todo.hdel(list.as_str(), key.to_string());
            return Ok(None);
        }
        Ok(Some(task))
    }
}
