use super::*;
use log::info;
use std::collections::HashMap;

impl<'a> Bouche<'a> {
    /// Get a new task to execute
    ///
    /// # Arguments
    /// * `db_conn` - The database connection
    /// * `timeout` - The maximum time to wait for a task
    ///
    /// # Return value
    /// The communication, if any
    pub(crate) async fn slave_get_task_internal(
        &self,
        db_conn: &diesel::PgConnection,
        timeout: Duration,
    ) -> Result<(Option<SelectCommunication>, Duration), InternalError> {
        let (task, time_spent_waiting): (Option<Uuid>, Duration) =
            self.get_new_task(timeout).await?;
        let task = match task {
            Some(x) => x,
            None => return Ok((None, time_spent_waiting)),
        };
        let comm: Option<SelectCommunication> = self.get_communication(&db_conn, &task).await?;
        let comm = match comm {
            Some(x) => x,
            None => {
                info!(
                    "Task {} doesn't exists in the database, delaying it...",
                    task
                );
                self.add_to_delayed_list(task).await?;
                return Ok((None, time_spent_waiting));
            }
        };
        Ok((Some(comm), time_spent_waiting))
    }

    /// Get a task that can be run by bouche
    ///
    /// # Arguments
    /// * `db_conn` - The database connection
    /// * `timeout` - The maximum time to wait for a task
    ///
    /// # Return value
    /// The bouche task, if any
    pub(crate) async fn slave_get_runnable_task(
        &self,
        conn: &diesel::PgConnection,
        timeout: Duration,
    ) -> Result<(Option<BoucheTask>, Duration), InternalError> {
        let (comm, time_spent_waiting) = self.slave_get_task_internal(conn, timeout).await?;
        let comm = match comm {
            Some(x) => x,
            None => return Ok((None, time_spent_waiting)),
        };
        Ok((Some(BoucheTask::new(&conn, comm)?), time_spent_waiting))
    }

    pub(crate) async fn slave_fetch_and_run_task_internal(
        &self,
        conn: &diesel::PgConnection,
        timeout: Duration,
    ) -> Result<(BoucheTaskResult, Duration), InternalError> {
        let (task, time_spent_waiting) = self.slave_get_runnable_task(conn, timeout).await?;
        let task = match task {
            Some(x) => x,
            None => return Ok((BoucheTaskResult::new(), time_spent_waiting)),
        };
        Ok((
            self.slave_run_task_internal(conn, task).await?,
            time_spent_waiting,
        ))
    }

    /// Run the task, once it has been fetched
    ///
    /// # Arguments
    /// * `conn` - The database connection
    /// * `communication` - The task
    ///
    /// # Return value
    /// The result of the task
    pub async fn slave_run_task_internal(
        &self,
        conn: &diesel::PgConnection,
        task: BoucheTask,
    ) -> Result<BoucheTaskResult, InternalError> {
        let mut res: BoucheTaskResult = BoucheTaskResult::new();
        match self.send_mail(&conn, task).await? {
            BoucheMessageStatus::Success => res.add_success(),
            BoucheMessageStatus::NoRecipient => res.add_success(),
            BoucheMessageStatus::ShouldRetry(_x) => res.add_to_retry(),
            BoucheMessageStatus::Failure(_x) => res.add_failed(),
        };
        info!("Task result : {:#?}", res);
        Ok(res)
    }

    pub async fn slave_run_delayed_tasks_internal(
        &self,
        conn: &diesel::PgConnection,
    ) -> Result<BoucheTaskResult, InternalError> {
        let mut res = BoucheTaskResult::new();
        let mut index = 0;
        let list = BOUCHE_DELAYED_BACKLOG_FORMAT!(self.visage().id());

        while {
            let mut todo = visage::redis::pipe();
            // Scan for the 10 next element of the hashset
            let (new_index, tasks): (u64, HashMap<String, String>) = self
                .visage()
                .apply_cmd_in_redis(
                    visage::redis::cmd("HSCAN")
                        .arg(list.as_str())
                        .arg(index)
                        .arg("COUNT")
                        .arg(10 as i64),
                )
                .await?;
            index = new_index;
            // Run the routine
            for (key, task) in tasks {
                let mut task: BoucheDelayedTask = match self
                    .slave_parse_delayed_task(&mut res, &mut todo, &list, &key, &task)
                    .await?
                {
                    Some(x) => x,
                    None => continue,
                };
                if chrono::Utc::now() < *task.not_before() {
                    res.add_to_retry();
                    continue;
                }
                if *task.retry_nb() >= self.config().max_retry_nb {
                    self.remove_from_delayed_list(*task.id()).await?;
                    res.add_failed();
                    continue;
                }
                let comm: Option<SelectCommunication> =
                    self.get_communication(&conn, task.id()).await?;
                match comm {
                    Some(x) => {
                        res = res
                            + self
                                .slave_run_task_internal(&conn, BoucheTask::new(&conn, x)?)
                                .await?;
                        self.remove_from_delayed_list(*task.id()).await?;
                    }
                    None => {
                        task.bump(self.config());
                        todo.hset(
                            list.as_str(),
                            task.id().to_string(),
                            serde_json::to_string(&task)?,
                        );
                    }
                }
            }
            // Apply all the processed tasks
            self.visage().apply_pipe_in_redis(&mut todo).await?;
            // Quit the loop when the cursor is 0 (end)
            index != 0
        } {}
        Ok(res)
    }

    /// Run the delayed tasks
    ///
    ///
    /// # Return value
    /// The result of the task
    pub async fn slave_run_delayed_tasks(&self) -> Result<BoucheTaskResult, InternalError> {
        self.slave_run_delayed_tasks_internal(&*self.visage().db_conn().get()?)
            .await
    }

    /// Fetch a task and run it, if a task is available in `timeout` at most.
    ///
    /// # Arguments
    /// * `timeout` - The maximum time to wait for a task
    ///
    /// # Return value
    /// Some statistics about the task, if any
    pub async fn slave_fetch_and_run_task(
        &self,
        timeout: Duration,
    ) -> Result<(BoucheTaskResult, Duration), InternalError> {
        self.slave_fetch_and_run_task_internal(&*self.visage().db_conn().get()?, timeout)
            .await
    }
}
