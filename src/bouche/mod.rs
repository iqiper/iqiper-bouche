#[macro_export]
macro_rules! BOUCHE_BACKLOG_FORMAT {
    ($oeil_id:expr) => {
        format!("BBL_{}", $oeil_id)
    };
}

#[macro_export]
macro_rules! BOUCHE_DELAYED_BACKLOG_FORMAT {
    ($oeil_id:expr) => {
        format!("BDBL_{}", $oeil_id)
    };
}

use crate::configuration::Configuration;
use crate::errors::InternalError;
use chrono::{Duration, Utc};
use db::communication::SelectCommunication;
use db::communication_log::{NewCommunicationLog, SelectCommunicationLog};
use db::contact::SelectContact;
use db::enums::{CommunicationReasonType, CommunicationStatus};
use getset::{Getters, MutGetters};
use handlebars::Handlebars;
use log::{debug, error, warn};
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use uuid::Uuid;
use visage::Visage;
mod bouche;
mod bouche_mail;
mod bouche_registration;
mod bouche_slave;
mod bouche_utils;
#[cfg(test)]
pub mod tests;
const BOUCHE_REGISTRAR: &'static str = "BOUCHES";

const TMPL_MAIL_SUBJECT_CHECK: (&'static str, &'static str) = (
    "mail_subject_check",
    include_str!("../../templates/mail_subject_check.hbs"),
);
const TMPL_MAIL_BODY_CHECK: (&'static str, &'static str) = (
    "mail_body_check",
    include_str!("../../templates/mail_body_check.hbs"),
);

const TMPL_MAIL_SUBJECT_ALERT: (&'static str, &'static str) = (
    "mail_subject_alert",
    include_str!("../../templates/mail_subject_alert.hbs"),
);
const TMPL_MAIL_BODY_ALERT: (&'static str, &'static str) = (
    "mail_body_alert",
    include_str!("../../templates/mail_body_alert.hbs"),
);

const TMPL_MAIL_BODY_FORCED_ALERT: (&'static str, &'static str) = (
    "mail_body_forced_alert",
    include_str!("../../templates/mail_body_forced_alert.hbs"),
);
const TMPL_MAIL_SUBJECT_FORCED_ALERT: (&'static str, &'static str) = (
    "mail_subject_forced_alert",
    include_str!("../../templates/mail_subject_forced_alert.hbs"),
);

const TMPL_MAIL_BODY_CORRECTION: (&'static str, &'static str) = (
    "mail_body_correction",
    include_str!("../../templates/mail_body_correction.hbs"),
);
const TMPL_MAIL_SUBJECT_CORRECTION: (&'static str, &'static str) = (
    "mail_subject_correction",
    include_str!("../../templates/mail_subject_correction.hbs"),
);

use once_cell::sync::Lazy;
static TMPL: Lazy<Vec<(&'static str, &'static str)>> = Lazy::new(|| {
    vec![
        TMPL_MAIL_SUBJECT_CHECK,
        TMPL_MAIL_SUBJECT_ALERT,
        TMPL_MAIL_BODY_CHECK,
        TMPL_MAIL_BODY_ALERT,
        TMPL_MAIL_BODY_FORCED_ALERT,
        TMPL_MAIL_SUBJECT_FORCED_ALERT,
        TMPL_MAIL_BODY_CORRECTION,
        TMPL_MAIL_SUBJECT_CORRECTION,
    ]
});
pub use bouche::{
    Bouche, BoucheDelayedTask, BoucheMessageStatus, BoucheMessageTemplateData, BoucheMetrics,
    BoucheTDResult, BoucheTask, BoucheTaskResult,
};
