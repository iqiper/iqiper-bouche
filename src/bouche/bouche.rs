use super::*;
extern crate kcc as KCC;
use crate::kcc::init_kcc;
use crate::reqwest::init_reqwest_client;
use chrono::{DateTime, Duration, Utc};
use peau::prometheus::{Histogram, HistogramOpts, IntCounter, IntGauge, Registry};
#[derive(Clone, Debug)]
pub enum BoucheMessageStatus {
    Success,
    ShouldRetry(lettre::transport::smtp::response::Response),
    Failure(lettre::transport::smtp::response::Response),
    NoRecipient,
}

#[derive(Getters, MutGetters)]
pub struct Bouche<'a> {
    #[getset(get = "pub", get_mut = "pub")]
    visage: Visage,
    #[getset(get = "pub")]
    config: crate::configuration::ConfigurationBouche,
    #[getset(get = "pub(super)")]
    kcc: kcc::KeycloakClient,
    #[getset(get = "pub")]
    smtp: Box<
        dyn lettre::Transport<
            Ok = lettre::transport::smtp::response::Response,
            Error = lettre::transport::smtp::Error,
        >,
    >,
    #[getset(get = "pub")]
    handlebars: Handlebars<'a>,
    #[getset(get = "pub")]
    metrics: BoucheMetrics,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct BoucheTDResult {
    #[getset(get = "pub")]
    added: usize,
    #[getset(get = "pub")]
    updated: usize,
    #[getset(get = "pub")]
    removed: usize,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct BoucheTaskResult {
    #[getset(get = "pub")]
    success: usize,
    #[getset(get = "pub")]
    failed: usize,
    #[getset(get = "pub")]
    to_retry: usize,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct BoucheTask {
    #[getset(get = "pub")]
    communication: SelectCommunication,
    #[getset(get = "pub")]
    recipients: Vec<(SelectContact, Vec<SelectCommunicationLog>)>,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct BoucheDelayedTask {
    #[getset(get = "pub")]
    id: Uuid,
    #[getset(get = "pub")]
    retry_nb: u64,
    #[getset(get = "pub")]
    not_before: DateTime<Utc>,
}

#[derive(Getters, Clone, Debug, Serialize, Deserialize)]
pub struct BoucheMessageTemplateData<'a> {
    #[getset(get = "pub")]
    activity: Option<db::activity::SelectActivity>,
    #[getset(get = "pub")]
    user: Option<KCC::keycloak_types::UserRepresentation<'a>>,
    #[getset(get = "pub")]
    constraint: Option<db::activity_constraint::SelectActivityConstraint>,
    #[getset(get = "pub")]
    communication: SelectCommunication,
}

impl<'a> BoucheMessageTemplateData<'a> {
    pub fn new(
        communication: SelectCommunication,
        user: Option<KCC::keycloak_types::UserRepresentation<'a>>,
        activity: Option<db::activity::SelectActivity>,
        constraint: Option<db::activity_constraint::SelectActivityConstraint>,
    ) -> Self {
        BoucheMessageTemplateData {
            communication,
            activity,
            user,
            constraint,
        }
    }
}

#[derive(Getters, Clone, Debug)]
pub struct BoucheMetrics {
    #[getset(get = "pub")]
    saturation: IntGauge,
    #[getset(get = "pub")]
    loop_latencies: Histogram,
    #[getset(get = "pub")]
    retried_tasks: IntCounter,
    #[getset(get = "pub")]
    failed_tasks: IntCounter,
    #[getset(get = "pub")]
    success_tasks: IntCounter,
}

impl BoucheMetrics {
    pub fn new() -> Result<Self, InternalError> {
        Ok(BoucheMetrics {
            saturation: IntGauge::new(
                "saturation",
                "The time running every function in the main loop takes",
            )?,
            loop_latencies: Histogram::with_opts(HistogramOpts::new(
                "loop_latencies",
                "The time running a loop takes",
            ))?,
            retried_tasks: IntCounter::new(
                "retried_tasks",
                "The time it takes to fetch done tasks",
            )?,
            failed_tasks: IntCounter::new("failed_tasks", "The number of failed tasks")?,
            success_tasks: IntCounter::new("success_tasks", "The number of successful tasks")?,
        })
    }
    /// Register metrics
    pub fn register(&self, registry: &Registry) -> Result<(), InternalError> {
        registry.register(Box::new(self.saturation.clone()))?;
        registry.register(Box::new(self.loop_latencies.clone()))?;
        registry.register(Box::new(self.retried_tasks.clone()))?;
        registry.register(Box::new(self.failed_tasks.clone()))?;
        registry.register(Box::new(self.success_tasks.clone()))?;
        registry.register(Box::new(IntCounter::new("up", "Up status about a Bouche")?))?;
        Ok(())
    }
}

impl BoucheDelayedTask {
    pub fn new(config: &crate::configuration::ConfigurationBouche, id: Uuid) -> Self {
        BoucheDelayedTask {
            id,
            retry_nb: 0,
            not_before: Utc::now() + Duration::milliseconds(config.delay_between_retry as i64),
        }
    }

    pub fn bump(&mut self, config: &crate::configuration::ConfigurationBouche) {
        self.not_before = Utc::now() + Duration::milliseconds(config.delay_between_retry as i64);
        self.retry_nb = self.retry_nb + 1;
    }
}

impl BoucheTask {
    /// Check if a communication is done
    ///
    /// # Arguments
    /// * `logs` - The logs of the communication
    ///
    /// # Return value
    /// True if the communication is done
    pub fn check_if_communication_is_done(logs: &Vec<SelectCommunicationLog>) -> bool {
        for log in logs.iter() {
            match log.status {
                CommunicationStatus::Failed | CommunicationStatus::Sent => return false,
                CommunicationStatus::Retrying => (),
            };
        }
        return true;
    }

    /// Sort through the contacts for those who still need processing
    ///
    /// # Arguments
    /// * `contacts` - A vector of contacts with their logs
    ///
    /// # Return value
    /// The new vector of contacts with their logs, but only those requiring processing
    pub fn sort_contact(
        contacts: Vec<(SelectContact, Vec<SelectCommunicationLog>)>,
    ) -> Result<Vec<(SelectContact, Vec<SelectCommunicationLog>)>, InternalError> {
        Ok(contacts
            .into_iter()
            .filter(|(_contact, logs)| BoucheTask::check_if_communication_is_done(&logs))
            .collect())
    }

    /// Create a new bouche object
    ///
    /// # Arguments
    /// * `db_conn` - The database connection object
    /// * `communication` - The communication that is represented by this task
    ///
    /// # Return value
    /// The BoucheTask
    pub fn new(
        db_conn: &diesel::PgConnection,
        communication: SelectCommunication,
    ) -> Result<Self, InternalError> {
        let contact: Vec<SelectContact> =
            db::communication_recipient::select_by_communication_id_with_contacts(
                db_conn,
                &communication.uid,
                &communication.id,
            )?;
        let recipients: Vec<(SelectContact, Vec<SelectCommunicationLog>)> =
            BoucheTask::sort_contact(db::communication_log::select_by_contacts_unlogged(
                db_conn,
                &communication.id,
                contact,
            )?)?;
        Ok(BoucheTask {
            communication,
            recipients,
        })
    }
}

impl BoucheTaskResult {
    pub fn new() -> Self {
        BoucheTaskResult {
            success: 0,
            failed: 0,
            to_retry: 0,
        }
    }

    pub fn add_success(&mut self) {
        self.success = self.success + 1;
    }

    pub fn add_failed(&mut self) {
        self.failed = self.failed + 1;
    }

    pub fn add_to_retry(&mut self) {
        self.to_retry = self.to_retry + 1;
    }
}

impl std::ops::Add<BoucheTaskResult> for BoucheTaskResult {
    type Output = BoucheTaskResult;

    fn add(self, rhs: BoucheTaskResult) -> BoucheTaskResult {
        let mut res = BoucheTaskResult::new();
        res.failed = self.failed + rhs.failed();
        res.success = self.success + rhs.success();
        res.to_retry = self.to_retry + rhs.to_retry();
        res
    }
}

impl<'a> Bouche<'a> {
    /// Read the template to use to send the messages
    ///
    /// # Return value
    /// The Handlebar object used for templating
    fn read_templates() -> Result<Handlebars<'a>, InternalError> {
        let mut handlebars = Handlebars::new();
        for (name, tmpl) in TMPL.iter() {
            handlebars.register_template_string(name, tmpl)?;
        }
        Ok(handlebars)
    }

    pub fn handle_metrics(&self, res: &BoucheTaskResult) {
        self.metrics().success_tasks().inc_by(*res.success() as i64);
        self.metrics().failed_tasks().inc_by(*res.failed() as i64);
        self.metrics()
            .retried_tasks()
            .inc_by(*res.to_retry() as i64);
    }

    /// Configure the smtp client
    ///
    /// # Arguments
    /// * `config` - The configuration of `bouche`
    ///
    /// # Return value
    /// The smtp transport object
    fn configure_smtp(
        config: &Configuration,
    ) -> Result<
        Box<
            dyn lettre::Transport<
                Ok = lettre::transport::smtp::response::Response,
                Error = lettre::transport::smtp::Error,
            >,
        >,
        InternalError,
    > {
        let mut smtp = lettre::SmtpTransport::relay(config.bouche.smtp.host.as_str())?
            .hello_name(lettre::transport::smtp::extension::ClientId::Domain(
                config.bouche.smtp.hello_name.clone(),
            ))
            .port(config.bouche.smtp.port);
        if let Some(creds) = &config.bouche.smtp.credentials {
            smtp = smtp
                .credentials(lettre::transport::smtp::authentication::Credentials::new(
                    creds.username.clone(),
                    creds.password.clone(),
                ))
                .authentication(vec![
                    lettre::transport::smtp::authentication::Mechanism::Plain,
                    lettre::transport::smtp::authentication::Mechanism::Login,
                ]);
        }
        if let Some(creds) = &config.bouche.smtp.credentials {
            smtp = smtp.credentials(lettre::transport::smtp::authentication::Credentials::new(
                creds.username.clone(),
                creds.password.clone(),
            ));
        }
        if let Some(tls) = &config.bouche.smtp.tls {
            let mut tls_creds = lettre::transport::smtp::client::TlsParameters::builder(
                config.bouche.smtp.host.clone(),
            );
            if let Some(root_cert) = &tls.root_cert {
                let certificate = std::fs::read_to_string(root_cert)?;
                tls_creds.add_root_certificate(
                    lettre::transport::smtp::client::Certificate::from_pem(certificate.as_bytes())?,
                );
            }
            if tls.strict {
                tls_creds.dangerous_accept_invalid_certs(true);
                tls_creds.dangerous_accept_invalid_hostnames(true);
            }
            smtp = smtp.tls(lettre::transport::smtp::client::Tls::Wrapper(
                tls_creds.build()?,
            ));
        } else {
            smtp = smtp.tls(lettre::transport::smtp::client::Tls::None);
        }
        Ok(std::boxed::Box::new(smtp.build()))
    }

    /// Create a new bouche object providing a `smtp` object
    ///
    /// # Arguments
    /// * `config` - The configuration of `bouche`
    /// * `smtp` - The configured smtp object
    ///
    /// # Return value
    /// The bouche object
    pub async fn new_with_smtp(
        config: Configuration,
        smtp: Box<
            dyn lettre::Transport<
                Ok = lettre::transport::smtp::response::Response,
                Error = lettre::transport::smtp::Error,
            >,
        >,
        metrics: BoucheMetrics,
    ) -> Result<Bouche<'a>, InternalError> {
        let reqwest_client = init_reqwest_client(&config)?;
        let kcc = init_kcc(&config, reqwest_client).await;
        Ok(Bouche {
            visage: Visage::new(config.visage.clone(), BOUCHE_REGISTRAR).await?,
            smtp,
            config: config.bouche.clone(),
            kcc,
            handlebars: Bouche::read_templates()?,
            metrics,
        })
    }

    /// Create a new bouche object
    ///
    /// # Arguments
    /// * `config` - The configuration of `bouche`
    /// * `metrics` - The metrics object to use
    ///
    /// # Return value
    /// The bouche object
    pub async fn new(
        config: Configuration,
        metrics: BoucheMetrics,
    ) -> Result<Bouche<'a>, InternalError> {
        let smtp = Bouche::configure_smtp(&config)?;
        Bouche::new_with_smtp(config, smtp, metrics).await
    }
}
