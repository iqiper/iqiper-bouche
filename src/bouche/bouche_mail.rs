use super::*;
use log::info;

impl<'a> Bouche<'a> {
    pub(super) async fn send_mail_internal(
        &self,
        mail: &lettre::Message,
    ) -> Result<BoucheMessageStatus, InternalError> {
        debug!("Sending mail to {:#?}", mail.envelope().to());
        match self.smtp().send(mail) {
            Ok(_x) => Ok(BoucheMessageStatus::Success),
            Err(lettre::transport::smtp::Error::Transient(x)) => {
                warn!("[SMTP] Transient error {:#?}", x);
                Ok(BoucheMessageStatus::ShouldRetry(x))
            }
            Err(lettre::transport::smtp::Error::Permanent(x)) => {
                warn!("[SMTP] Permanent error {:#?}", x);
                Ok(BoucheMessageStatus::Failure(x))
            }
            Err(x) => {
                error!("SMTP Transport failed : {}", x);
                Err(InternalError::LettreSmtpError(x))
            }
        }
    }

    /// Fetch the email of a contact using the database or Keycloak if necessary.
    ///
    /// # Arguments
    /// * `conn` - The database connection
    /// * `task` - The task to send
    ///
    /// # Return value
    /// A list of mailbox to send the email to
    pub(super) async fn extract_email_from_contact(
        &self,
        conn: &diesel::PgConnection,
        task: &BoucheTask,
    ) -> Result<lettre::message::Mailboxes, InternalError> {
        let mut mailboxes = lettre::message::Mailboxes::new();
        for contact in task.recipients().iter() {
            match &contact.0.email {
                Some(email) => match lettre::Address::from_str(email.as_str()) {
                    Ok(email) => mailboxes.push(lettre::message::Mailbox::new(None, email)),
                    Err(err) => {
                        warn!(
                            "Discarding email {} for communication {} : {}",
                            email,
                            task.communication().id,
                            err
                        );
                        db::communication_log::create(
                            &conn,
                            db::communication_log::NewCommunicationLog {
                                communication_id: task.communication().id,
                                contact_id: contact.0.id,
                                status: db::enums::CommunicationStatus::Failed,
                            },
                        )?;
                    }
                },
                None => match contact.0.target_uid {
                    Some(id) => match self.kcc().search_user_by_id(id).await {
                        Ok(user) => match user.email {
                            Some(email) => {
                                match lettre::Address::from_str(email.to_string().as_str()) {
                                    Ok(email) => {
                                        mailboxes.push(lettre::message::Mailbox::new(None, email))
                                    }
                                    Err(err) => {
                                        warn!("Discarding email (from Keycloak) {} for communication {} : {}", email, task.communication().id, err);
                                        db::communication_log::create(
                                            &conn,
                                            db::communication_log::NewCommunicationLog {
                                                communication_id: task.communication().id,
                                                contact_id: contact.0.id,
                                                status: db::enums::CommunicationStatus::Failed,
                                            },
                                        )?;
                                    }
                                }
                            }
                            None => {
                                warn!("Failed to get email from Keycloak for communication {} and user {}", task.communication().id, id);
                                db::communication_log::create(
                                    &conn,
                                    db::communication_log::NewCommunicationLog {
                                        communication_id: task.communication().id,
                                        contact_id: contact.0.id,
                                        status: db::enums::CommunicationStatus::Failed,
                                    },
                                )?;
                            }
                        },
                        Err(kcc::KCCError::NotFound(_x)) => {
                            warn!("The user {} doesn't exists anymore in Keycloak for communication {}", id, task.communication().id);
                            db::communication_log::create(
                                &conn,
                                db::communication_log::NewCommunicationLog {
                                    communication_id: task.communication().id,
                                    contact_id: contact.0.id,
                                    status: db::enums::CommunicationStatus::Failed,
                                },
                            )?;
                        }
                        Err(err) => {
                            error!("There was a Keycloak error while trying to fetch an user email (user : {}, communication : {})", id, task.communication().id);
                            db::communication_log::create(
                                &conn,
                                db::communication_log::NewCommunicationLog {
                                    communication_id: task.communication().id,
                                    contact_id: contact.0.id,
                                    status: db::enums::CommunicationStatus::Failed,
                                },
                            )?;
                            return Err(InternalError::KCCError(err));
                        }
                    },
                    None => {
                        error!("Bad record, there should be either an email or an uid");
                    }
                },
            }
        }
        Ok(mailboxes)
    }

    /// Create the mail object
    ///
    /// # Arguments
    /// * `conn` - The database connection
    /// * `task` - The task to send
    ///
    /// # Return value
    /// The SMTP Message builder if any
    async fn create_mail_object(
        &self,
        conn: &diesel::PgConnection,
        task: &BoucheTask,
    ) -> Result<Option<lettre::message::MessageBuilder>, InternalError> {
        let contacts = self.extract_email_from_contact(conn, &task).await?;
        let mut iter = contacts.iter();
        if iter.next().is_none() {
            return Ok(None);
        }
        let mut res = lettre::message::Message::builder()
            .header(lettre::message::header::ContentType::text_utf8())
            .sender(self.config().smtp.addresses.from.clone())
            .from(self.config().smtp.addresses.from.clone())
            .reply_to(self.config().smtp.addresses.reply_to.clone());
        for contact in contacts.into_iter() {
            res = res.bcc(contact);
        }
        Ok(Some(res))
    }

    /// Populate the mail message, setting the body of the message using the templates.
    ///
    /// # Arguments
    /// * `task` - The task to send
    /// * `mails` - The message builder currently beeing built
    ///
    /// # Return value
    /// The SMTP message to send
    async fn populate_mail_message(
        &self,
        db_conn: &diesel::PgConnection,
        task: &BoucheTask,
        mails: lettre::message::MessageBuilder,
    ) -> Result<lettre::message::Message, InternalError> {
        let subject: String;
        let body: String;
        let data = self.get_template_data(db_conn, task).await?;
        match task.communication().reason {
            CommunicationReasonType::Check => {
                subject = self.handlebars().render(TMPL_MAIL_SUBJECT_CHECK.0, &data)?;
                body = self.handlebars().render(TMPL_MAIL_BODY_CHECK.0, &data)?;
            }
            CommunicationReasonType::Alert => {
                subject = self.handlebars().render(TMPL_MAIL_SUBJECT_ALERT.0, &data)?;
                body = self.handlebars().render(TMPL_MAIL_BODY_ALERT.0, &data)?;
            }
            CommunicationReasonType::ForcedAlert => {
                subject = self
                    .handlebars()
                    .render(TMPL_MAIL_SUBJECT_FORCED_ALERT.0, &data)?;
                body = self
                    .handlebars()
                    .render(TMPL_MAIL_BODY_FORCED_ALERT.0, &data)?;
            }
            CommunicationReasonType::Correction => {
                subject = self
                    .handlebars()
                    .render(TMPL_MAIL_SUBJECT_CORRECTION.0, &data)?;
                body = self
                    .handlebars()
                    .render(TMPL_MAIL_BODY_CORRECTION.0, &data)?;
            }
        }
        let body: String = body.chars().filter(|c| c.is_ascii()).collect::<String>();
        Ok(mails.subject(subject).body(body)?)
    }

    /// Handle the result when sending an email. Setting the communication logs
    /// in the database.
    ///
    /// # Arguments
    /// * `conn` - The database connection
    /// * `task` - The task to send
    /// * `result` - The status or error returned when trying to send the email
    ///
    /// # Return value
    /// Nothing
    async fn mail_handle_result(
        &self,
        conn: &diesel::PgConnection,
        task: &BoucheTask,
        result: &Result<BoucheMessageStatus, InternalError>,
    ) -> Result<(), InternalError> {
        match &result {
            Ok(BoucheMessageStatus::Success) => {
                info!(
                    "Successfully sent communication {}",
                    task.communication().id
                );
                db::communication_log::create_multi(
                    conn,
                    task.recipients()
                        .into_iter()
                        .map(|(contact, _logs)| NewCommunicationLog {
                            communication_id: task.communication().id,
                            contact_id: contact.id,
                            status: CommunicationStatus::Sent,
                        })
                        .collect(),
                )?;
            }
            Ok(BoucheMessageStatus::NoRecipient) => {
                warn!(
                    "No recipients for communication {}",
                    task.communication().id
                );
            }
            Ok(BoucheMessageStatus::ShouldRetry(_x)) => {
                warn!(
                    "Task {} failed but should be retried. It'll be queued again",
                    task.communication().id
                );
                let res = self
                    .visage()
                    .apply_cmd_in_redis(&mut visage::redis::Cmd::lpush(
                        self.config().backlog_name.as_str(),
                        task.communication().id.to_string(),
                    ))
                    .await?;
                db::communication_log::create_multi(
                    conn,
                    task.recipients()
                        .into_iter()
                        .map(|(contact, _logs)| NewCommunicationLog {
                            communication_id: task.communication().id,
                            contact_id: contact.id,
                            status: CommunicationStatus::Retrying,
                        })
                        .collect(),
                )?;
                res
            }
            Ok(BoucheMessageStatus::Failure(err)) => {
                warn!(
                    "Sending communication {} failed : {:#?}",
                    task.communication().id,
                    err
                );
                db::communication_log::create_multi(
                    conn,
                    task.recipients()
                        .into_iter()
                        .map(|(contact, _logs)| NewCommunicationLog {
                            communication_id: task.communication().id,
                            contact_id: contact.id,
                            status: CommunicationStatus::Failed,
                        })
                        .collect(),
                )?;
            }
            Err(err) => {
                error!(
                    "IO Error while delivering email, task {} will be queued again : {}",
                    task.communication().id,
                    err
                );
                let res = self
                    .visage()
                    .apply_cmd_in_redis(&mut visage::redis::Cmd::lpush(
                        self.config().backlog_name.as_str(),
                        task.communication().id.to_string(),
                    ))
                    .await?;
                db::communication_log::create_multi(
                    conn,
                    task.recipients()
                        .into_iter()
                        .map(|(contact, _logs)| NewCommunicationLog {
                            communication_id: task.communication().id,
                            contact_id: contact.id,
                            status: CommunicationStatus::Retrying,
                        })
                        .collect(),
                )?;
                res
            }
        }
        Ok(())
    }

    /// Send an email
    ///
    /// # Arguments
    /// * `conn` - The database connection
    /// * `task` - The task to send
    ///
    /// # Return value
    /// The message status
    pub(crate) async fn send_mail(
        &self,
        conn: &diesel::PgConnection,
        task: BoucheTask,
    ) -> Result<BoucheMessageStatus, InternalError> {
        info!("Sending communication {}", task.communication().id);
        let mail_builder = self.create_mail_object(conn, &task).await?;
        let mail_builder = match mail_builder {
            Some(x) => x,
            None => {
                self.delete_task(
                    BOUCHE_BACKLOG_FORMAT!(self.visage().id()),
                    task.communication().id.to_string(),
                )
                .await?;
                warn!(
                    "No recipients for communication {}",
                    task.communication().id
                );
                return Ok(BoucheMessageStatus::NoRecipient);
            }
        };
        let mail = self
            .populate_mail_message(conn, &task, mail_builder)
            .await?;
        let res = self.send_mail_internal(&mail).await;
        self.delete_task(
            BOUCHE_BACKLOG_FORMAT!(self.visage().id()),
            task.communication().id.to_string(),
        )
        .await?;
        self.mail_handle_result(&conn, &task, &res).await?;
        Ok(res?)
    }
}
