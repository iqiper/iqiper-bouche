use super::*;
use diesel::Connection;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id = create_contact(&db_conn, &uid);
            let _activity_contact_id =
                create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, comm_id.to_string()).await;
            let _activity = master_bouche
                .slave_get_task_internal(&db_conn, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            visage::redis::Cmd::del(visage::VISAGE_ID_FORMAT!(master_bouche.visage().id()))
                .query_async::<_, usize>(&mut bouche_test.test_conn)
                .await
                .unwrap();
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered (again)");
            master_bouche
                .master_check_orphaned_bouche()
                .await
                .expect("to put back the still tasks on the backlog");
            let res = visage::redis::Cmd::lpop(bouche_test.config.bouche.backlog_name.as_str())
                .query_async::<_, String>(&mut bouche_test.test_conn)
                .await
                .unwrap();
            assert_eq!(res.to_string(), comm_id.to_string(), "id don't match");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn bad_formatted() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            add_task(&mut bouche_test, "AAAAAAAAAAAAAAAAAAAAAAAAAAA".to_string()).await;
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let _activity = master_bouche
                .slave_get_task_internal(&db_conn, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            visage::redis::Cmd::zadd(
                BOUCHE_REGISTRAR,
                master_bouche.visage().id().to_string(),
                0 as usize,
            )
            .query_async::<_, usize>(&mut bouche_test.test_conn)
            .await
            .unwrap();
            master_bouche
                .master_check_orphaned_bouche()
                .await
                .expect("to put back the still tasks on the backlog");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
