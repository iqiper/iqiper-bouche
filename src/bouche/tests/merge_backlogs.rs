use super::*;
use diesel::Connection;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let list_a = String::from("A");
            let list_b = String::from("B");
            add_task_to_list(&mut bouche_test, list_a.clone(), "1".to_string()).await;
            add_task_to_list(&mut bouche_test, list_a.clone(), "2".to_string()).await;
            add_task_to_list(&mut bouche_test, list_a.clone(), "3".to_string()).await;
            add_task_to_list(&mut bouche_test, list_b.clone(), "4".to_string()).await;
            add_task_to_list(&mut bouche_test, list_b.clone(), "5".to_string()).await;
            add_task_to_list(&mut bouche_test, list_b.clone(), "6".to_string()).await;
            master_bouche
                .master_merge_backlogs(&list_b.clone(), &list_a.clone())
                .await
                .expect("To have merged the lists");
            let res: Vec<usize> = visage::redis::pipe()
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .query_async(&mut bouche_test.test_conn)
                .await
                .expect("reading new list");
            for (i, v) in res.iter().enumerate() {
                assert_eq!(6 - i, *v, "Should be equal");
            }
            Ok(())
        })
    });
}

#[test]
fn old_dont_exists() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let list_a = String::from("A");
            add_task_to_list(&mut bouche_test, list_a.clone(), "1".to_string()).await;
            add_task_to_list(&mut bouche_test, list_a.clone(), "2".to_string()).await;
            add_task_to_list(&mut bouche_test, list_a.clone(), "3".to_string()).await;
            master_bouche
                .master_merge_backlogs(&"AAAAAA".to_string(), &list_a.clone())
                .await
                .expect("To have merged the lists");
            let res: Vec<usize> = visage::redis::pipe()
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .lpop(list_a.clone())
                .query_async(&mut bouche_test.test_conn)
                .await
                .expect("reading new list");
            for (i, v) in res.iter().enumerate() {
                assert_eq!(3 - i, *v, "Should be equal");
            }
            Ok(())
        })
    });
}

#[test]
fn new_dont_exists() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let list_a = String::from("A");
            add_task_to_list(&mut bouche_test, list_a.clone(), "1".to_string()).await;
            add_task_to_list(&mut bouche_test, list_a.clone(), "2".to_string()).await;
            add_task_to_list(&mut bouche_test, list_a.clone(), "3".to_string()).await;
            master_bouche
                .master_merge_backlogs(&list_a.clone(), &"AAAAAA".to_string())
                .await
                .expect("To have merged the lists");
            let res: Vec<usize> = visage::redis::pipe()
                .lpop(&"AAAAAA".to_string())
                .lpop(&"AAAAAA".to_string())
                .lpop(&"AAAAAA".to_string())
                .query_async(&mut bouche_test.test_conn)
                .await
                .expect("reading new list");
            for (i, v) in res.iter().enumerate() {
                assert_eq!(3 - i, *v, "Should be equal");
            }
            Ok(())
        })
    });
}
