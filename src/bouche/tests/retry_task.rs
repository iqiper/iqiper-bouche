use super::*;
use diesel::Connection;
use lettre::transport::smtp::response::Category as SMTPResponseCategory;
use lettre::transport::smtp::response::Code as SMTPResponseCode;
use lettre::transport::smtp::response::Detail as SMTPResponseDetail;
use lettre::transport::smtp::response::Response as SMTPResponse;
use lettre::transport::smtp::response::Severity as SMTPResponseSeverity;
use test_db::*;

async fn create_user_and_constraint(db_conn: &PgConnection) -> (Uuid, Uuid) {
    let mut bouche_test = setup().await;
    let uid = create_test_user(&db_conn);
    let activity_id = create_activity(
        &db_conn,
        &uid,
        false,
        Some(Utc::now() - Duration::minutes(1)),
        None,
    );
    let constraint_id = create_activity_constraint(
        &db_conn,
        &uid,
        &activity_id,
        false,
        Some(Utc::now()),
        None,
        Some(Utc::now() + Duration::minutes(2)),
        None,
    );
    let contact_id = create_contact(&db_conn, &uid);
    let _activity_contact_id = create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
    bouche_test.config.visage.master.enable = false;
    bouche_test.config.visage.slave.enable = true;
    bouche_test.config.visage.master.enable = true;
    bouche_test.config.visage.rates.ttl_lease = 60;
    (uid, constraint_id)
}

async fn create_test_bouche(config: crate::configuration::Configuration) -> Bouche<'static> {
    Bouche::new_with_smtp(
        config,
        Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
            SMTPResponseCode::new(
                SMTPResponseSeverity::PositiveCompletion,
                SMTPResponseCategory::Information,
                SMTPResponseDetail::Zero,
            ),
            vec![],
        ))),
        OEIL_METRICS.clone(),
    )
    .await
    .expect("the bouche should've been built correctly")
}

#[test]
fn simple_retry() {
    let db_conn = establish_connection();
    let db_conn_second = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, constraint_id) = create_user_and_constraint(&db_conn).await;
            bouche_test.config.bouche.delay_between_retry = 0; // NO DELAY
            bouche_test.config.bouche.max_retry_nb = 10; // RETRY
            let mut bouche = create_test_bouche(bouche_test.config.clone()).await;
            bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            add_task(&mut bouche_test, comm_id.to_string()).await;
            let (res, _dur) = bouche
                .slave_fetch_and_run_task_internal(&db_conn_second, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(*res.success(), 0, "There should be 0 success task");
            let res = bouche
                .slave_run_delayed_tasks_internal(&db_conn)
                .await
                .expect("the new task to be fetched");
            assert_eq!(*res.success(), 1, "There should be 1 success task");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn max_retry() {
    let db_conn = establish_connection();
    let db_conn_second = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, constraint_id) = create_user_and_constraint(&db_conn).await;
            bouche_test.config.bouche.delay_between_retry = 0; // NO DELAY
            bouche_test.config.bouche.max_retry_nb = 0; // RETRY
            let mut bouche = create_test_bouche(bouche_test.config.clone()).await;
            bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            add_task(&mut bouche_test, comm_id.to_string()).await;
            let (res, _dur) = bouche
                .slave_fetch_and_run_task_internal(&db_conn_second, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(*res.success(), 0, "There should be 0 success task");
            let res = bouche
                .slave_run_delayed_tasks_internal(&db_conn_second)
                .await
                .expect("the new task to be fetched");
            assert_eq!(*res.success(), 0, "There should be 0 success task");
            let res = bouche
                .slave_run_delayed_tasks_internal(&db_conn)
                .await
                .expect("the new task to be fetched");
            assert_eq!(*res.success(), 0, "There should be 0 success task");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
