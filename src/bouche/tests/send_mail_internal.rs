use super::*;
use diesel::Connection;
use lettre::transport::smtp::response::Category as SMTPResponseCategory;
use lettre::transport::smtp::response::Code as SMTPResponseCode;
use lettre::transport::smtp::response::Detail as SMTPResponseDetail;
use lettre::transport::smtp::response::Response as SMTPResponse;
use lettre::transport::smtp::response::Severity as SMTPResponseSeverity;
use std::boxed::Box;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let message = lettre::Message::builder()
                .to(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .from(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .subject("eeee")
                .body("toto")
                .unwrap();
            let resp = master_bouche
                .send_mail_internal(&message)
                .await
                .expect("To have send the mail");
            match resp {
                BoucheMessageStatus::Success => (),
                _ => unimplemented!(),
            };
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn retryable() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_err(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::TransientNegativeCompletion,
                        SMTPResponseCategory::MailSystem,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let message = lettre::Message::builder()
                .to(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .from(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .subject("eeee")
                .body("toto")
                .unwrap();
            let resp = master_bouche
                .send_mail_internal(&message)
                .await
                .expect("To have send the mail");
            match resp {
                BoucheMessageStatus::ShouldRetry(_x) => (),
                _ => unimplemented!(),
            };
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn failure() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_err(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PermanentNegativeCompletion,
                        SMTPResponseCategory::MailSystem,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let message = lettre::Message::builder()
                .to(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .from(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .subject("eeee")
                .body("toto")
                .unwrap();
            let resp = master_bouche
                .send_mail_internal(&message)
                .await
                .expect("To have send the mail");
            match resp {
                BoucheMessageStatus::Failure(_x) => (),
                _ => unimplemented!(),
            };
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn failure_other() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_io_error()),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let message = lettre::Message::builder()
                .to(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .from(lettre::message::Mailbox::from_str("coucou@exmapl.com").unwrap())
                .subject("eeee")
                .body("toto")
                .unwrap();
            master_bouche
                .send_mail_internal(&message)
                .await
                .expect_err("to have failed");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
