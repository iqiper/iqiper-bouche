use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let comm_id = db::communication::create_from_constraint(
                &db_conn,
                &constraint_id,
                db::communication::NewCommunication {
                    uid,
                    type_: db::enums::CommunicationType::Email,
                    reason: db::enums::CommunicationReasonType::Check,
                },
            )
            .unwrap()
            .id;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, comm_id.to_string()).await;
            let (new_task, _dur) = master_bouche
                .get_new_task(Duration::seconds(1))
                .await
                .expect("the new task to be fetched");
            assert_eq!(new_task.is_some(), true, "Should be some");
            assert_eq!(new_task.unwrap(), comm_id, "Ids should match");
            let task: Option<String> =
                visage::redis::Cmd::lpop(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .unwrap();
            assert_eq!(task.is_some(), true, "It should be some");
            assert_eq!(
                task.unwrap(),
                comm_id.to_string(),
                "Ids should match in new list"
            );
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn with_null_timeout() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let comm_id = db::communication::create_from_constraint(
                &db_conn,
                &constraint_id,
                db::communication::NewCommunication {
                    uid,
                    type_: db::enums::CommunicationType::Email,
                    reason: db::enums::CommunicationReasonType::Check,
                },
            )
            .unwrap()
            .id;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, comm_id.to_string()).await;
            let (new_task, _dur) = master_bouche
                .get_new_task(Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(new_task.is_some(), true, "Should be some");
            assert_eq!(new_task.unwrap(), comm_id, "Ids should match");
            let task: Option<String> =
                visage::redis::Cmd::lpop(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .unwrap();
            assert_eq!(task.is_some(), true, "It should be some");
            assert_eq!(
                task.unwrap(),
                comm_id.to_string(),
                "Ids should match in new list"
            );
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn with_null_timeout_and_no_task() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let (new_task, _dur) = master_bouche
                .get_new_task(Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(new_task.is_none(), true, "Should be none");
            let task: Option<String> =
                visage::redis::Cmd::lpop(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .unwrap();
            assert_eq!(task.is_none(), true, "It should be none");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn with_no_task() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let (new_task, _dur) = master_bouche
                .get_new_task(Duration::seconds(1))
                .await
                .expect("the new task to be fetched");
            assert_eq!(new_task.is_none(), true, "Should be none");
            let task: Option<String> =
                visage::redis::Cmd::lpop(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .unwrap();
            assert_eq!(task.is_none(), true, "It should be none");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn bad_uuid() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, String::from("AAAAAAAAAAAAAA")).await;
            let (new_task, _dur) = master_bouche
                .get_new_task(Duration::seconds(1))
                .await
                .expect("the new task to be fetched");
            assert_eq!(new_task.is_none(), true, "Should be none");
            let task: Option<String> =
                visage::redis::Cmd::lpop(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .unwrap();
            assert_eq!(task.is_none(), true, "It should be none");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
