use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id = create_contact(&db_conn, &uid);
            let _activity_contact_id =
                create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = db::communication::create_from_constraint(
                &db_conn,
                &id,
                NewCommunication {
                    reason: CommunicationReasonType::Check,
                    type_: CommunicationType::Email,
                    uid: uid,
                },
            )
            .expect("to create the communication")
            .id;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, comm_id.to_string()).await;
            let (res, _dur) = master_bouche
                .slave_get_task_internal(&db_conn, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(res.is_some(), true, "Should be some");
            let comm = res.unwrap();
            assert_eq!(comm.id, comm_id, "Ids should match");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn bad_id() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, "AAAAAAAAAA".to_string()).await;
            let (activity, _dur) = master_bouche
                .slave_get_task_internal(&db_conn, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(activity.is_none(), true, "Should be none");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn cant_find_constraint() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, Uuid::new_v4().to_string()).await;
            let (activity, _dur) = master_bouche
                .slave_get_task_internal(&db_conn, Duration::seconds(0))
                .await
                .expect("the new task to be fetched");
            assert_eq!(activity.is_none(), true, "Should be none");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
