use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, id.to_string()).await;
            let res: bool = master_bouche
                .delete_task(
                    bouche_test.config.bouche.backlog_name.clone(),
                    id.to_string(),
                )
                .await
                .expect("to have delete the task");
            assert_eq!(res, true, "Should have deleted");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn list_not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, id.to_string()).await;
            let res: bool = master_bouche
                .delete_task("AAAAAAAAAAAAA".to_string(), id.to_string())
                .await
                .expect("to have delete the task");
            assert_eq!(res, false, "Should have deleted");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn element_not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            add_task(&mut bouche_test, id.to_string()).await;
            let res: bool = master_bouche
                .delete_task(
                    bouche_test.config.bouche.backlog_name.clone(),
                    "AAAAAAAAAAAAA".to_string(),
                )
                .await
                .expect("to have delete the task");
            assert_eq!(res, false, "Should have deleted");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
