use super::*;
use diesel::Connection;
use diesel::PgConnection;
use lettre::transport::smtp::response::Category as SMTPResponseCategory;
use lettre::transport::smtp::response::Code as SMTPResponseCode;
use lettre::transport::smtp::response::Detail as SMTPResponseDetail;
use lettre::transport::smtp::response::Response as SMTPResponse;
use lettre::transport::smtp::response::Severity as SMTPResponseSeverity;
use std::boxed::Box;
use test_db::*;

async fn create_user_in_keycloak_with_email<'a>(
    db_conn: &PgConnection,
    bouche: &Bouche<'a>,
    email: &str,
) -> Uuid {
    let user_id = bouche
        .kcc()
        .create_user(kcc::keycloak_types::UserRepresentation {
            first_name: Some(std::borrow::Cow::Borrowed(
                Uuid::new_v4().to_string().as_str(),
            )),
            last_name: Some(std::borrow::Cow::Borrowed(
                Uuid::new_v4().to_string().as_str(),
            )),
            username: Some(std::borrow::Cow::Borrowed(email)),
            email: Some(std::borrow::Cow::Borrowed(email)),
            email_verified: Some(true),
            enabled: Some(true),
            credentials: Some(vec![kcc::keycloak_types::CredentialRepresentation {
                type_: Some(std::borrow::Cow::Borrowed("password")),
                temporary: Some(false),
                value: Some(std::borrow::Cow::Borrowed("toto")),
                ..Default::default()
            }]),
            ..Default::default()
        })
        .await
        .expect("To create the user in keycloak");
    db::distant_user::create_user(&db_conn, &user_id).expect("user creation failed");
    user_id
}

#[test]
fn ok_contact_email() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id =
                create_custom_contact_email(&db_conn, &uid, String::from("hello@world"));
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id(&db_conn, &uid, &comm_id).unwrap(),
            )
            .unwrap();
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let mailboxes = master_bouche
                .extract_email_from_contact(&db_conn, &task)
                .await
                .expect("To have found the email");
            let mailbox = mailboxes.into_single().unwrap();
            assert_eq!(
                mailbox.email.to_string().as_str(),
                "hello@world",
                "emails mismatch"
            );
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn contact_email_bad_address() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id =
                create_custom_contact_email(&db_conn, &uid, String::from("helloworld"));
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id(&db_conn, &uid, &comm_id).unwrap(),
            )
            .unwrap();
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let mailboxes = master_bouche
                .extract_email_from_contact(&db_conn, &task)
                .await
                .expect("To have found the email");
            mailboxes
                .into_single()
                .ok_or(())
                .expect_err("There shouldn't be an address");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn contact_friend_ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let friend_email = format!("{}@{}.com", Uuid::new_v4(), Uuid::new_v4());
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let friend_uid =
                create_user_in_keycloak_with_email(&db_conn, &master_bouche, friend_email.as_str())
                    .await;
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id = create_custom_contact_friend(&db_conn, &uid, friend_uid);
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id(&db_conn, &uid, &comm_id).unwrap(),
            )
            .unwrap();
            let mailboxes = master_bouche
                .extract_email_from_contact(&db_conn, &task)
                .await
                .expect("To have found the email");
            let mailbox = mailboxes.into_single().unwrap();
            assert_eq!(
                mailbox.email.to_string().as_str(),
                friend_email.as_str(),
                "emails mismatch"
            );
            master_bouche.kcc().destroy_user(friend_uid).await.unwrap();
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn contact_friend_bad_email() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let friend_email = format!("{}{}.com", Uuid::new_v4(), Uuid::new_v4());
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let friend_uid =
                create_user_in_keycloak_with_email(&db_conn, &master_bouche, friend_email.as_str())
                    .await;
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id = create_custom_contact_friend(&db_conn, &uid, friend_uid);
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id(&db_conn, &uid, &comm_id).unwrap(),
            )
            .unwrap();
            let mailboxes = master_bouche
                .extract_email_from_contact(&db_conn, &task)
                .await
                .expect("To have found the email");
            mailboxes
                .into_single()
                .ok_or(())
                .expect_err("There shouldn't be an address");
            master_bouche.kcc().destroy_user(friend_uid).await.unwrap();
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn contact_friend_no_email() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let friend_uid = master_bouche
                .kcc()
                .create_user(kcc::keycloak_types::UserRepresentation {
                    first_name: Some(std::borrow::Cow::Borrowed(
                        Uuid::new_v4().to_string().as_str(),
                    )),
                    last_name: Some(std::borrow::Cow::Borrowed(
                        Uuid::new_v4().to_string().as_str(),
                    )),
                    username: Some(std::borrow::Cow::Borrowed(
                        Uuid::new_v4().to_string().as_str(),
                    )),
                    email: None,
                    email_verified: None,
                    enabled: Some(true),
                    credentials: Some(vec![kcc::keycloak_types::CredentialRepresentation {
                        type_: Some(std::borrow::Cow::Borrowed("password")),
                        temporary: Some(false),
                        value: Some(std::borrow::Cow::Borrowed("toto")),
                        ..Default::default()
                    }]),
                    ..Default::default()
                })
                .await
                .expect("To create the user in keycloak");
            db::distant_user::create_user(&db_conn, &friend_uid).expect("user creation failed");
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id = create_custom_contact_friend(&db_conn, &uid, friend_uid);
            let _activity_contact_id =
                create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id(&db_conn, &uid, &comm_id).unwrap(),
            )
            .unwrap();
            let mailboxes = master_bouche
                .extract_email_from_contact(&db_conn, &task)
                .await
                .expect("To have found the email");
            mailboxes
                .into_single()
                .ok_or(())
                .expect_err("There shouldn't be an address");
            master_bouche.kcc().destroy_user(friend_uid).await.unwrap();
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn contact_friend_not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let friend_uid = Uuid::new_v4();
            db::distant_user::create_user(&db_conn, &friend_uid).expect("user creation failed");
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let contact_id = create_custom_contact_friend(&db_conn, &uid, friend_uid);
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Alert,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id(&db_conn, &uid, &comm_id).unwrap(),
            )
            .unwrap();
            let mailboxes = master_bouche
                .extract_email_from_contact(&db_conn, &task)
                .await
                .expect("To have found the email");
            mailboxes
                .into_single()
                .ok_or(())
                .expect_err("There shouldn't be an address");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
