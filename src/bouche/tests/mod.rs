use crate::bouche::*;
use db::communication::NewCommunication;
use db::enums::{CommunicationReasonType, CommunicationType};
mod bouche_stub_mail;
mod delete_task;
mod extract_email_from_contact;
mod get_communication;
mod get_task;
mod get_task_internal;
mod merge_backlogs;
mod reassignation_on_death;
mod retry_task;
mod send_mail;
mod send_mail_internal;
use async_std::task;
use bouche_stub_mail::BoucheStubStmp;
use chrono::Utc;
use diesel::PgConnection;
use once_cell::sync::Lazy;
use std::sync::{Mutex, MutexGuard};
use uuid::Uuid;

static OEIL_METRICS: Lazy<BoucheMetrics> = Lazy::new(|| BoucheMetrics::new().unwrap());

const CONF_FILE: &str = "IQIPER_BOUCHE_CONFIG";
const ENV_CONF_VAR: &str = "IQIPER_BOUCHE";
const NB_REDIS_DB: u8 = 16;

pub struct BoucheTest {
    pub db: MutexGuard<'static, u8>,
    pub config: crate::configuration::Configuration,
    pub test_client: visage::redis::Client,
    pub test_conn: visage::redis::aio::ConnectionManager,
    pub db_conn: diesel::PgConnection,
}

pub async fn setup() -> BoucheTest {
    let db = lock_db().await;
    let mut config = get_config();
    let db_conn = test_db::establish_connection();
    config.visage.redis.database = Some((*db).to_string());
    let (test_client, mut test_conn) =
        visage::connections::redis::get_redis_connection(&config.visage.redis)
            .await
            .expect("The connection failed to be established");
    visage::connections::redis::test_connection(&mut test_conn)
        .await
        .expect("The connection is invalid");
    let flush_res: bool = visage::redis::cmd("FLUSHDB")
        .query_async(&mut test_conn)
        .await
        .expect("Flushed fails");
    assert_eq!(flush_res, true, "Clean up wen't wrong");
    BoucheTest {
        db,
        config,
        test_client,
        test_conn,
        db_conn,
    }
}

pub async fn clean_up(bouche_setup: &mut BoucheTest) {
    let res: bool = visage::redis::cmd("FLUSHDB")
        .query_async(&mut bouche_setup.test_conn)
        .await
        .expect("Flushed fails");
    assert_eq!(res, true, "Clean up went wrong");
}

pub async fn add_task(bouche_setup: &mut BoucheTest, task: String) {
    let list = bouche_setup.config.bouche.backlog_name.clone();
    add_task_to_list(bouche_setup, list, task).await
}

pub async fn add_task_to_list(bouche_setup: &mut BoucheTest, list: String, task: String) {
    let _e: bool = visage::redis::Cmd::lpush(list, task)
        .query_async(&mut bouche_setup.test_conn)
        .await
        .expect("LPUSH fails");
}

static DB_POOL: Lazy<Vec<Mutex<u8>>> = Lazy::new(|| {
    let mut res: Vec<Mutex<u8>> = vec![];
    for i in 0..NB_REDIS_DB {
        res.push(Mutex::new(i));
    }
    res
});

pub fn get_config() -> crate::configuration::Configuration {
    crate::configuration::get(
        std::path::Path::new(
            std::env::var(CONF_FILE)
                .unwrap_or("./config.yml".to_string())
                .as_str(),
        ),
        ENV_CONF_VAR,
    )
    .unwrap()
}

fn create_communication(
    db_conn: &PgConnection,
    uid: &Uuid,
    constraint_id: &Uuid,
    reason: CommunicationReasonType,
) -> Uuid {
    db::communication::create_from_constraint(
        &db_conn,
        &constraint_id,
        db::communication::NewCommunication {
            uid: *uid,
            reason,
            type_: db::enums::CommunicationType::Email,
        },
    )
    .unwrap()
    .id
}

pub async fn lock_db() -> MutexGuard<'static, u8> {
    loop {
        let mut nb_poisoned = 0;
        let mut nb_pool = 0;
        for db in DB_POOL.iter() {
            nb_pool = nb_pool + 1;
            if db.is_poisoned() {
                nb_poisoned = nb_poisoned + 1;
            }
            let tmp = db.try_lock();
            if tmp.is_ok() {
                return tmp.unwrap();
            }
            if nb_poisoned == nb_pool {
                panic!("[redis] Every db lock is poisoned");
            }
        }
        task::sleep(std::time::Duration::from_millis(200)).await;
    }
}
