use super::*;
use chrono::{Duration, Utc};
use diesel::Connection;
use test_db::*;

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let uid = create_test_user(&db_conn);
            let activity_id = create_activity(
                &db_conn,
                &uid,
                false,
                Some(Utc::now() - Duration::minutes(1)),
                None,
            );
            let constraint_id = create_activity_constraint(
                &db_conn,
                &uid,
                &activity_id,
                false,
                Some(Utc::now()),
                None,
                Some(Utc::now() + Duration::minutes(2)),
                None,
            );
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                db::enums::CommunicationReasonType::Check,
            );
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let res = master_bouche
                .get_communication(&db_conn, &comm_id)
                .await
                .expect("to fetch from db");
            assert_eq!(res.is_some(), true, "It should be some");
            assert_ne!(res.unwrap().id, activity_id, "Ids shouldn't match");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn not_found() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let mut master_bouche = Bouche::new(bouche_test.config.clone(), OEIL_METRICS.clone())
                .await
                .expect("the bouche should've been built correctly");
            master_bouche
                .visage_mut()
                .register_lease()
                .await
                .expect("should be registered");
            let res = master_bouche
                .get_communication(&db_conn, &Uuid::new_v4())
                .await
                .expect("to fetch from db");
            assert_eq!(res.is_none(), true, "It should be none");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
