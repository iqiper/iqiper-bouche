use super::*;
use db::communication::SelectCommunication;
use db::enums::{CommunicationReasonType, CommunicationStatus};
use diesel::Connection;
use diesel::PgConnection;
use lettre::transport::smtp::response::Category as SMTPResponseCategory;
use lettre::transport::smtp::response::Code as SMTPResponseCode;
use lettre::transport::smtp::response::Detail as SMTPResponseDetail;
use lettre::transport::smtp::response::Response as SMTPResponse;
use lettre::transport::smtp::response::Severity as SMTPResponseSeverity;
use std::boxed::Box;
use test_db::*;

fn create_constraint(db_conn: &PgConnection) -> (Uuid, Uuid, Uuid) {
    let uid = create_test_user(&db_conn);
    let activity_id = create_activity(
        &db_conn,
        &uid,
        false,
        Some(Utc::now() - Duration::minutes(1)),
        None,
    );
    let id = create_activity_constraint(
        &db_conn,
        &uid,
        &activity_id,
        false,
        Some(Utc::now()),
        None,
        Some(Utc::now() + Duration::minutes(2)),
        None,
    );
    (uid, activity_id, id)
}

fn check_logs_for_recipients(
    db_conn: &PgConnection,
    communication: SelectCommunication,
    expected_status: CommunicationStatus,
) {
    let contact: Vec<SelectContact> =
        db::communication_recipient::select_by_communication_id_with_contacts(
            db_conn,
            &communication.uid,
            &communication.id,
        )
        .unwrap();
    let recipients: Vec<(SelectContact, Vec<SelectCommunicationLog>)> = BoucheTask::sort_contact(
        db::communication_log::select_by_contacts_unlogged(db_conn, &communication.id, contact)
            .unwrap(),
    )
    .unwrap();
    'recipients: for (contact, logs) in recipients.into_iter() {
        for log in logs.into_iter() {
            if log.status == expected_status {
                continue 'recipients;
            }
        }
        panic!(
            "The status {} is not set for {}",
            expected_status, contact.id
        );
    }
}

#[test]
fn ok() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, activity_id, constraint_id) = create_constraint(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let contact_id =
                create_custom_contact_email(&db_conn, &uid, "hello@world.com".to_string());
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                CommunicationReasonType::Check,
            );

            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id_unlogged(&db_conn, &comm_id).unwrap(),
            )
            .unwrap();
            let _r: usize = visage::redis::Cmd::lpush(
                BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()),
                task.communication().id.to_string(),
            )
            .query_async(&mut bouche_test.test_conn)
            .await
            .expect("To execute the redis command correctly");

            let resp = master_bouche
                .send_mail(&db_conn, task.clone())
                .await
                .expect("It should have tried to send the mail");
            match resp {
                BoucheMessageStatus::Success => (),
                _ => unimplemented!(),
            };
            check_logs_for_recipients(
                &db_conn,
                task.communication().clone(),
                CommunicationStatus::Sent,
            );
            let res: usize =
                visage::redis::Cmd::llen(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn no_recipient() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, _activity_id, constraint_id) = create_constraint(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_ok(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PositiveCompletion,
                        SMTPResponseCategory::Information,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                CommunicationReasonType::Check,
            );

            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id_unlogged(&db_conn, &comm_id).unwrap(),
            )
            .unwrap();
            let _r: usize = visage::redis::Cmd::lpush(
                BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()),
                task.communication().id.to_string(),
            )
            .query_async(&mut bouche_test.test_conn)
            .await
            .expect("To execute the redis command correctly");

            let resp = master_bouche
                .send_mail(&db_conn, task)
                .await
                .expect("It should have tried to send the mail");
            match resp {
                BoucheMessageStatus::NoRecipient => (),
                _ => unimplemented!(),
            };
            let res: usize =
                visage::redis::Cmd::llen(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn retry() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, activity_id, constraint_id) = create_constraint(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_err(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::TransientNegativeCompletion,
                        SMTPResponseCategory::MailSystem,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let contact_id =
                create_custom_contact_email(&db_conn, &uid, "hello@world.com".to_string());
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                CommunicationReasonType::Check,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id_unlogged(&db_conn, &comm_id).unwrap(),
            )
            .unwrap();
            let _r: usize = visage::redis::Cmd::lpush(
                BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()),
                task.communication().id.to_string(),
            )
            .query_async(&mut bouche_test.test_conn)
            .await
            .expect("To execute the redis command correctly");

            let resp = master_bouche
                .send_mail(&db_conn, task.clone())
                .await
                .expect("It should have tried to send the mail");
            match resp {
                BoucheMessageStatus::ShouldRetry(_x) => (),
                _ => unimplemented!(),
            };
            check_logs_for_recipients(
                &db_conn,
                task.communication().clone(),
                CommunicationStatus::Retrying,
            );
            let res: usize =
                visage::redis::Cmd::llen(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");

            let res: usize =
                visage::redis::Cmd::llen(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");
            let res: usize =
                visage::redis::Cmd::llen(bouche_test.config.bouche.backlog_name.as_str())
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 1, "The backlog shouldn't be empty");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn failed_smtp() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, activity_id, constraint_id) = create_constraint(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_err(SMTPResponse::new(
                    SMTPResponseCode::new(
                        SMTPResponseSeverity::PermanentNegativeCompletion,
                        SMTPResponseCategory::MailSystem,
                        SMTPResponseDetail::Zero,
                    ),
                    vec![],
                ))),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let contact_id =
                create_custom_contact_email(&db_conn, &uid, "hello@world.com".to_string());
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                CommunicationReasonType::Check,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id_unlogged(&db_conn, &comm_id).unwrap(),
            )
            .unwrap();
            let _r: usize = visage::redis::Cmd::lpush(
                BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()),
                task.communication().id.to_string(),
            )
            .query_async(&mut bouche_test.test_conn)
            .await
            .expect("To execute the redis command correctly");

            let resp = master_bouche
                .send_mail(&db_conn, task.clone())
                .await
                .expect("It should have tried to send the mail");
            match resp {
                BoucheMessageStatus::Failure(_x) => (),
                _ => unimplemented!(),
            };
            check_logs_for_recipients(
                &db_conn,
                task.communication().clone(),
                CommunicationStatus::Failed,
            );
            let res: usize =
                visage::redis::Cmd::llen(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");
            let res: usize =
                visage::redis::Cmd::llen(bouche_test.config.bouche.backlog_name.as_str())
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}

#[test]
fn failed_io() {
    let db_conn = establish_connection();
    db_conn.test_transaction::<_, diesel::result::Error, _>(|| {
        tokio_test::block_on(async {
            let mut bouche_test = setup().await;
            let (uid, activity_id, constraint_id) = create_constraint(&db_conn);
            bouche_test.config.visage.master.enable = false;
            bouche_test.config.visage.slave.enable = true;
            bouche_test.config.visage.master.enable = true;
            bouche_test.config.visage.rates.ttl_lease = 60;
            let master_bouche = Bouche::new_with_smtp(
                bouche_test.config.clone(),
                Box::new(BoucheStubStmp::new_io_error()),
                OEIL_METRICS.clone(),
            )
            .await
            .expect("the bouche should've been built correctly");
            let contact_id =
                create_custom_contact_email(&db_conn, &uid, "hello@world.com".to_string());
            create_activity_contact(&db_conn, &uid, &activity_id, &contact_id);
            let comm_id = create_communication(
                &db_conn,
                &uid,
                &constraint_id,
                CommunicationReasonType::Check,
            );
            let task = BoucheTask::new(
                &db_conn,
                db::communication::select_by_id_unlogged(&db_conn, &comm_id).unwrap(),
            )
            .unwrap();
            let _r: usize = visage::redis::Cmd::lpush(
                BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()),
                task.communication().id.to_string(),
            )
            .query_async(&mut bouche_test.test_conn)
            .await
            .expect("To execute the redis command correctly");

            let _resp = master_bouche
                .send_mail(&db_conn, task.clone())
                .await
                .expect_err("It should have tried to send the mail, but failed");
            check_logs_for_recipients(
                &db_conn,
                task.communication().clone(),
                CommunicationStatus::Retrying,
            );
            let res: usize =
                visage::redis::Cmd::llen(BOUCHE_BACKLOG_FORMAT!(master_bouche.visage().id()))
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 0, "The backlog should be empty");
            let res: usize =
                visage::redis::Cmd::llen(bouche_test.config.bouche.backlog_name.as_str())
                    .query_async(&mut bouche_test.test_conn)
                    .await
                    .expect("To execute the redis command correctly");
            assert_eq!(res, 1, "The backlog shouldn't be empty");
            clean_up(&mut bouche_test).await;
            Ok(())
        })
    });
}
