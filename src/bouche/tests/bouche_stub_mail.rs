use lettre::address::Envelope;
use lettre::transport::smtp::response::Response;
use lettre::transport::smtp::Error;
use lettre::Transport;

/// This transport logs the message envelope and returns the given response
#[derive(Debug)]
pub struct BoucheStubStmp {
    response: Option<Response>,
    is_error: bool,
}

impl BoucheStubStmp {
    /// Creates aResult new transport that always returns the given response
    pub fn new_ok(response: Response) -> BoucheStubStmp {
        BoucheStubStmp {
            response: Some(response),
            is_error: false,
        }
    }
    /// Creates aResult new transport that always returns the given response
    pub fn new_err(response: Response) -> BoucheStubStmp {
        BoucheStubStmp {
            response: Some(response),
            is_error: true,
        }
    }
    /// Creates aResult new transport that always returns the given response
    pub fn new_io_error() -> BoucheStubStmp {
        BoucheStubStmp {
            response: None,
            is_error: false,
        }
    }
}

impl Transport for BoucheStubStmp {
    type Ok = Response;
    type Error = Error;

    fn send_raw(&self, _envelope: &Envelope, _email: &[u8]) -> Result<Self::Ok, Self::Error> {
        match &self.response {
            Some(resp) => {
                if self.is_error {
                    Err(Error::from(resp.clone()))
                } else {
                    Ok(resp.clone())
                }
            }
            None => Err(Error::Resolution),
        }
    }
}
