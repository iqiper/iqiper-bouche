use crate::errors::InternalError;
use serde::Deserialize;
use visage::configuration as visage_config;

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationClientTls {
    pub key: String,
    pub crt: String,
    pub ca: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationTls {
    pub strict: bool,
    pub root_cert: Option<String>,
    pub identity: Option<ConfigurationClientTls>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationBoucheMailAddress {
    pub reply_to: lettre::message::Mailbox,
    pub from: lettre::message::Mailbox,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationUserPass {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationBoucheMail {
    pub tls: Option<ConfigurationTls>,
    pub hello_name: String,
    pub host: String,
    pub port: u16,
    pub credentials: Option<ConfigurationUserPass>,
    pub addresses: ConfigurationBoucheMailAddress,
    pub notification_template: String,
    pub alert_template: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationBouche {
    pub backlog_name: String,
    pub max_retry_nb: u64,
    pub delay_between_retry: u64,
    pub smtp: ConfigurationBoucheMail,
    pub keycloak: ConfigurationKeycloak,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationKeycloak {
    pub tls: Option<ConfigurationTls>,
    pub host: String,
    pub scheme: String,
    pub port: u32,
    pub realm: String,
    pub client_id: String,
    pub client_secret: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Configuration {
    pub visage: visage_config::Configuration,
    pub bouche: ConfigurationBouche,
    pub server: peau::configuration::ConfigurationServer,
}

/// Get the configuration from a path and the environment
pub fn get(path: &std::path::Path, env_var: &str) -> Result<Configuration, InternalError> {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::from(path))?
        .merge(config::Environment::with_prefix(env_var).separator("__"))?;
    Ok(settings.try_into::<Configuration>()?)
}
