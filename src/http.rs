use crate::errors::InternalError;
use getset::{Getters, MutGetters};
use log::error;
use peau::actix_web;
use peau::actix_web::post;
use peau::actix_web::web;
use peau::actix_web::HttpResponse;
use std::sync::{Arc, RwLock};
use uuid::Uuid;

#[derive(Clone, Getters, MutGetters)]
pub struct RedisCommunicator {
    #[getset(get = "pub")]
    client: visage::redis::Client,
    #[getset(get = "pub", get_mut = "pub")]
    conn: Arc<RwLock<visage::redis::aio::ConnectionManager>>,
    #[getset(get = "pub")]
    backlog_name: String,
}

impl RedisCommunicator {
    /// Create a new RedisCommunicator object
    ///
    /// # Arguments
    /// * `config` - The redis connection
    /// * `backlog_name` - The name of the `bouche` backlog
    ///
    /// # Return value
    /// The RedisCommunicator object
    pub async fn new(
        config: &visage::configuration::ConfigurationRedis,
        backlog_name: String,
    ) -> Result<Self, InternalError> {
        let (client, conn) = visage::connections::redis::get_redis_connection(&config).await?;
        Ok(RedisCommunicator {
            client,
            backlog_name,
            conn: Arc::new(RwLock::new(conn)),
        })
    }

    /// Add a task to the `bouche` backlog
    ///
    /// # Arguments
    /// * `task_id` - The id of the task
    ///
    /// # Return value
    /// Nothing
    pub async fn add_task(&self, task_id: Uuid) -> Result<(), InternalError> {
        visage::redis::Cmd::lpush(self.backlog_name().as_str(), task_id.to_string())
            .query_async(&mut *self.conn().write().map_err(|_e| {
                visage::errors::VisageError::LockPoisonError(String::from("redis connection"))
            })?)
            .await?;
        Ok(())
    }
}

/// POST /communication/{id}
/// Add a new task to the bouche backlog
#[post("/communication/{id}")]
pub(crate) async fn add_communication(
    path: web::Path<Uuid>,
    redis_comm: web::Data<Arc<RedisCommunicator>>,
) -> HttpResponse {
    let id = *path;
    redis_comm.add_task(id).await.unwrap_or_else(|err| {
        error!("Error while adding task {} :\n{}", id, err);
        HttpResponse::InternalServerError().finish();
        return ();
    });
    HttpResponse::Ok().finish()
}

/// Set the route for the communication feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(add_communication);
}
