use crate::configuration::Configuration;
use iqiper_tealer::oidc::{ClientId, ClientSecret, IssuerUrl};
use iqiper_tealer::{CoreIqiperTealerActor, IqiperTealer};
use kcc::KeycloakClient;
use log::info;
use std::sync::{Arc, RwLock};

/// Init the Keycloak Client
///
/// # Arguments
/// * `config` - The configuration object
/// * `reqwest_client` - The reqwest_client to use
///
/// # Return value
/// The Keycloak Client
pub async fn init_kcc(config: &Configuration, reqwest_client: reqwest::Client) -> KeycloakClient {
    info!("Initializing the tealer...");
    let tealer = IqiperTealer::new_client_grant(
        reqwest_client.clone(),
        IssuerUrl::new(format!(
            "{}://{}:{}/auth/realms/{}",
            config.bouche.keycloak.scheme,
            config.bouche.keycloak.host,
            config.bouche.keycloak.port,
            config.bouche.keycloak.realm
        ))
        .expect("Should've correctly built the IdP URL"),
        ClientId::new(config.bouche.keycloak.client_id.clone()),
        Some(ClientSecret::new(
            config.bouche.keycloak.client_secret.clone(),
        )),
    )
    .await
    .expect("Should've discovered the IdP correctly");
    info!("Initializing KCC...");
    let tealer_actor = Arc::new(RwLock::new(CoreIqiperTealerActor::new(tealer)));
    let kcc = KeycloakClient::new_shared(
        tealer_actor.clone(),
        reqwest_client.clone(),
        format!(
            "{}://{}:{}",
            config.bouche.keycloak.scheme, config.bouche.keycloak.host, config.bouche.keycloak.port
        ),
        config.bouche.keycloak.realm.clone(),
    );
    kcc
}
