//! `iqiper-bouche` is a service used to send message to `iqiper` users.
//!
//! ## Redis
//!
//! It uses redis to fetch new tasks and redistribute them in case of failure.
//! There is a single "registrar" which is basically a list from which every `bouche`
//! will try to read from. It then move the task to its own list and executes it.
//!
//! - If the task succeed, it'll delete the task from its list.
//! - If the task fails, in a recoverable manner, it'll put back the task to
//! the end of the backlog list
//! - If the task fails, in a definitive manner, it'll delete the task.
//!
//! In any of the above cases, the `bouche` will set the according logs for the
//! task.
//!
//! ## Transports
//!
//! ### SMTP
//!
//! For the moment the only implemented transport is through SMTP.
//! It'll will find the contacts for which the communication was not sent before
//! sending, preventing doublons.
//!

#[allow(unused_imports)]
extern crate openssl;
mod bouche;
mod configuration;
mod errors;
mod http;
mod kcc;
mod reqwest;

#[cfg(test)]
mod tests;

use bouche::BoucheMetrics;
use bouche::{Bouche, BoucheTaskResult};
use chrono::SubsecRound;
use chrono::{DateTime, Duration, Utc};
use errors::InternalError;
use log::{debug, info, warn};
use peau::actix_web;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
const CONF_FILE: &str = "IQIPER_BOUCHE_CONFIG";
const ENV_CONF_VAR: &str = "IQIPER_BOUCHE";

macro_rules! PERSIST_BOUCHE {
    ($bouche:expr, $start_date:expr) => {
        let now = Utc::now();
        if now.round_subsecs(1)
            > ($start_date + Duration::milliseconds($bouche.visage().config().rates.tick_rate))
                .round_subsecs(1)
        {
            warn!(
                "The bouche is running slow ! {} ms late",
                (now - $start_date
                    - Duration::milliseconds($bouche.visage().config().rates.tick_rate))
                .num_milliseconds()
            );
            $bouche.visage().update_lease().await?;
        }
    };
}

fn get_timeout(bouche: &Bouche<'_>, start_date: &DateTime<Utc>) -> Option<Duration> {
    let now = Utc::now();
    if now >= *start_date + Duration::milliseconds(bouche.visage().config().rates.tick_rate) {
        return None;
    }
    Some((*start_date + Duration::milliseconds(bouche.visage().config().rates.tick_rate)) - now)
}

/// Loop to execute when running as master.
/// It does the following :
///
/// 1. Check for orphaned bouche
///
/// Between each points of the list above it refreshes
/// registration of the running `oeil`
///
/// # Arguments
/// * `bouche` - The oeil object registered in `redis`
/// * `start_date` - The at which the loop had started.
///
/// # Return Value
/// Nothing
async fn master_loop(
    bouche: &Bouche<'static>,
    start_date: &DateTime<Utc>,
) -> Result<(), InternalError> {
    bouche.master_check_orphaned_bouche().await?;
    PERSIST_BOUCHE!(bouche, *start_date);
    Ok(())
}

/// Loop to execute when running as slave.
/// It does the following :
///
/// 1. Check for new tasks
/// 2. Run the tasks
/// 3. Run delayed tasks
///
/// Between each points of the list above it refreshes
/// registration of the running `oeil`
///
/// # Arguments
/// * `bouche` - The bouche object registered in `redis`
/// * `start_date` - The at which the loop had started.
///
/// # Return Value
/// Nothing
async fn slave_loop(
    bouche: &Bouche<'static>,
    start_date: &DateTime<Utc>,
) -> Result<Duration, InternalError> {
    let task = bouche.slave_run_delayed_tasks().await?;
    bouche.handle_metrics(&task);
    PERSIST_BOUCHE!(bouche, *start_date);
    let timeout = get_timeout(bouche, start_date);
    let (task, time_spent_waiting) = match timeout {
        Some(timeout) => bouche.slave_fetch_and_run_task(timeout).await?,
        None => (BoucheTaskResult::new(), Duration::seconds(0)),
    };
    bouche.handle_metrics(&task);
    PERSIST_BOUCHE!(bouche, *start_date);

    Ok(time_spent_waiting)
}

/// Handle the signals
///
/// # Arguments
/// * `term_signal` - The atomic bool set to true went a stop signal is encountered
///
/// # Return Value
/// Nothing
fn handle_signals(term_signal: Arc<AtomicBool>) -> Result<(), InternalError> {
    signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&term_signal))?;
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&term_signal))?;
    unsafe {
        signal_hook::register(signal_hook::SIGINT, || {
            warn!("Received SIGINT. Will shutdown at the end of this tick...");
        })?;
        signal_hook::register(signal_hook::SIGTERM, || {
            warn!("Received SIGTERM. Will shutdown at the end of this tick...");
        })?;
    }
    Ok(())
}

/// Main loop of the program.
/// It does the following :
///
/// 1. Update the lease
/// 2. Run the master loop if the `bouche` is a master
/// 3. Run the slave loop if the `bouche` is a slave
/// 4. Sleep if the loop has run faster than the tick rate.
///
/// # Arguments
/// * `bouche` - The bouche object registered in `redis`
/// * `term_signal` - The atomic bool set to true went a stop signal is encountered
///
/// # Return Value
/// Nothing
async fn main_loop(
    bouche: &Bouche<'static>,
    term_signal: Arc<AtomicBool>,
) -> Result<(), InternalError> {
    let tick_rate = Duration::milliseconds(bouche.visage().config().rates.tick_rate);
    let tick_rate_throttled = Duration::milliseconds(
        bouche.visage().config().rates.tick_rate
            - ((bouche.visage().config().rates.tick_rate as f64 / 5 as f64) as f64).round() as i64,
    );
    bouche.metrics().saturation().set(0);
    while !term_signal.load(Ordering::Relaxed) {
        debug!("TICK!");
        let _timer = bouche.metrics().loop_latencies().start_timer();
        bouche.visage().update_lease().await?;
        let start_date = Utc::now();
        if bouche.visage().config().master.enable {
            master_loop(bouche, &start_date).await?;
        }
        if bouche.visage().config().slave.enable {
            let mut spent_time: Duration = Duration::seconds(0);
            while (Utc::now() - start_date) < tick_rate_throttled {
                let start_date_slave_loop = Utc::now();
                let time_modifier = slave_loop(bouche, &start_date).await?;
                let end_date_slave_loop = Utc::now();
                let run_time = end_date_slave_loop - (start_date_slave_loop + time_modifier);
                spent_time = spent_time + run_time;
            }
            bouche.metrics().saturation().set(
                (((spent_time.num_milliseconds() as f64
                    / tick_rate_throttled.num_milliseconds() as f64)
                    * 100 as f64) as f64)
                    .round() as i64,
            );
        }
        let end_date = Utc::now();
        let diff_date = end_date - start_date;
        if !bouche.visage().config().slave.enable {
            bouche
                .metrics()
                .saturation()
                .set(diff_date.num_milliseconds() / tick_rate.num_milliseconds());
            if diff_date < tick_rate {
                std::thread::sleep(
                    (tick_rate - diff_date)
                        .to_std()
                        .unwrap_or_else(|_e| std::time::Duration::from_secs(0)),
                );
            }
        }
    }
    Ok(())
}

#[macro_export]
macro_rules! iqiper_bouche_app {
    ($conf: expr, $redis_comm: expr, $metrics: expr) => {
        actix_web::App::new()
            .wrap(peau::actix_web::middleware::Logger::default())
            .wrap($metrics)
            .app_data(
                peau::actix_web::web::PathConfig::default()
                    .error_handler(|err, _| peau::actix_http::error::ErrorBadRequest(err)),
            )
            .app_data(
                peau::actix_web::web::QueryConfig::default()
                    .error_handler(|err, _| peau::actix_http::error::ErrorBadRequest(err)),
            )
            .data($redis_comm.clone())
            .configure(peau::web::health::alive)
            .configure(peau::web::health::ready)
            .configure(crate::http::set_routes)
    };
}

/// Setup the bouche object and its metrics
///
/// # Arguments
/// * `config` - The bouche configuration object
///
/// # Return Value
/// The bouche object and the actix middleware to expose metrics
async fn setup_bouche(
    config: &configuration::Configuration,
) -> Result<(Bouche<'static>, peau::actix_web_prom::PrometheusMetrics), InternalError> {
    let mut labels = std::collections::HashMap::new();
    labels.insert(
        "bouche_role".to_string(),
        visage::Visage::get_role(&config.visage).to_string(),
    );
    let bouche_metrics = BoucheMetrics::new()?;
    let bouche = Bouche::new(config.clone(), bouche_metrics.clone()).await?;
    labels.insert("bouche_id".to_string(), bouche.visage().id().to_string());
    let shared_registry =
        peau::prometheus::Registry::new_custom(Some(String::from("bouche")), Some(labels)).unwrap();
    bouche_metrics.register(&shared_registry)?;
    let metrics_http = peau::actix_web_prom::PrometheusMetrics::new_with_registry(
        shared_registry,
        "actix",
        Some("/-/metrics"),
        None,
    )
    .unwrap();
    Ok((bouche, metrics_http))
}

#[tokio::main]
async fn main() -> Result<(), InternalError> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug")).init();
    let term_signal = Arc::new(AtomicBool::new(false));
    let local_server_set = tokio::task::LocalSet::new();
    let _server_sys = actix_web::rt::System::run_in_tokio("actix_server", &local_server_set);
    handle_signals(term_signal.clone())?;
    info!("Reading configuration...");
    let config = configuration::get(
        std::path::Path::new(
            std::env::var(CONF_FILE)
                .unwrap_or("./config.yml".to_string())
                .as_str(),
        ),
        ENV_CONF_VAR,
    )?;
    let (mut bouche, metrics_http) = setup_bouche(&config).await?;
    let redis_comm = std::sync::Arc::new(
        crate::http::RedisCommunicator::new(
            &config.visage.redis,
            config.bouche.backlog_name.clone(),
        )
        .await?,
    );
    let server = actix_web::HttpServer::new(move || {
        iqiper_bouche_app!(config, redis_comm, metrics_http.clone())
    });
    let _server_control = peau::web::web::bind_web_server(&config.server, server)?.run();
    bouche.visage_mut().register_lease().await?;
    peau::web::health::set_ready();
    peau::web::health::set_alive(true);
    info!("Starting bouche {}", bouche.visage().id());
    let bouche_proc = main_loop(&bouche, term_signal);
    futures::try_join!(bouche_proc).map(|_| ())?;
    bouche.visage().unregister_lease(None).await?;
    actix_web::rt::System::current().stop_with_code(0);
    actix_web::rt::System::current().stop(); //TODO Find a cleaner way to shutdown actix
    Ok(())
}
