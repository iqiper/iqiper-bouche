FROM gcr.io/distroless/static:nonroot

COPY --chown=nonroot:nonroot ./target/x86_64-unknown-linux-musl/release/iqiper-bouche /app/iqiper-bouche

EXPOSE 8000 9000

CMD ["/app/iqiper-bouche"]
