#!/bin/bash -x

/usr/src/bouche/target/x86_64-unknown-linux-musl/debug/iqiper-bouche &
while true;
do
	inotifywait /.restart
	kill %%
	wait %%
	/usr/src/bouche/target/x86_64-unknown-linux-musl/debug/iqiper-bouche &
done

