#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

SUCCESS=0
FAILURE=0
IP=$(dig +short oreille.iqiper)
CONTACT_ID=$(restish $1 create_contact favorite: false, nickname: robert, email: load@test.local -rf "body" -s http://$IP:80/  | jq -r .id)
ACTIVITY_ID=$(restish $1 create_activity name: test -rf "body" -s http://$IP:80/ | jq -r .id)
for i in $(seq 1 $2)
do
	PIDS=()
	for j in $(seq 1 $3)
	do
		restish $1 alert_activity $ACTIVITY_ID > /dev/null 2>/dev/null &
		PIDS+=($!)
	done
	for pid in ${PIDS[@]}; do
  		wait ${pid}
    	if [ "$?" -eq 0 ]; then
    		SUCCESS=$(($SUCCESS+1))
    	else
    		FAILURE=$(($FAILURE+1))
    	fi
		echo [$(($i*$3))]
	done
done

echo "Success : $SUCCESS\nFailure: $FAILURE"
